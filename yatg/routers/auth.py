"""Authentication router."""

import logging
from typing import Annotated

import httpx
import msgspec
from asyncpg import Connection
from httpx import URL
from jsonrpcclient import Ok
from litestar import Request, Router, get, post
from litestar.contrib.htmx.response import HTMXTemplate
from litestar.enums import RequestEncodingType
from litestar.exceptions.http_exceptions import HTTPException, NotAuthorizedException
from litestar.params import Body
from litestar.response import Redirect, Template
from litestar.status_codes import HTTP_303_SEE_OTHER, HTTP_400_BAD_REQUEST
from passlib.context import CryptContext

from yatg.clients.deluge import DelugeClient
from yatg.clients.rtorrent import RtorrentClient
from yatg.models import SignupForm, TorrentClient

logger = logging.getLogger(__name__)


@get("/login", name="login_get")
async def login_get() -> Template:
    """Render the login form."""
    return Template(template_name="login.html")


class LoginFormData(msgspec.Struct):
    """Login form data."""

    username: str
    password: str


pwd_context = CryptContext(schemes=["argon2"], default="argon2")


def _verify_password(plain_password: str, hashed_password: str) -> bool:
    verified: bool = pwd_context.verify(plain_password, hashed_password)
    return verified


async def _authenticate_user(
    connection: Connection, username: str, password: str
) -> tuple[str, int]:
    try:
        user_db = await connection.fetchrow(
            """select username, password_hash, id from users where username = $1""",
            username,
        )
        if not user_db:
            raise NotAuthorizedException
        username, hashed_password, user_id = user_db
        if not _verify_password(password, hashed_password):
            raise NotAuthorizedException
        return username, user_id
    except Exception as e:
        raise NotAuthorizedException from e


@post("/login", name="login_post")
async def login_post(
    request: Request,
    asyncpg_connection: Connection,
    data: Annotated[LoginFormData, Body(media_type=RequestEncodingType.URL_ENCODED)],
) -> Redirect | Template:
    """Authenticate the user and redirect to the dashboard."""
    username, user_id = await _authenticate_user(
        connection=asyncpg_connection, username=data.username, password=data.password
    )
    all_settings = await asyncpg_connection.fetch(
        """select s.id, s.client, s.uri from settings s join users u on u.id = s.user_id where u.username = $1""",  # noqa: E501
        username,
    )
    if all_settings == []:
        return Redirect("/settings", status_code=HTTP_303_SEE_OTHER)
    request.set_session({"username": username, "user_id": user_id})

    for setting in all_settings:
        setting_id, client, uri = setting
        uri = httpx.URL(uri)
        match client:
            case TorrentClient.rtorrent:
                request.app.state.clients[setting_id] = RtorrentClient(URL(uri))
            case TorrentClient.deluge:  # pragma: no cover
                request.app.state.clients[setting_id] = DelugeClient(URL(uri))
                await request.app.state.clients[setting_id].connect()
    return Redirect("/torrents", status_code=HTTP_303_SEE_OTHER)


@get("/rtorrent_check", name="rtorrent_check")
async def rtorrent_check(
    uri: str,
) -> HTMXTemplate:
    """Check if the socket is valid."""
    try:
        url = httpx.URL(uri)
        async with RtorrentClient(url) as rtorrent_client:
            h = await rtorrent_client.send_daemon(
                method="system.hostname", params=("",)
            )
            if isinstance(h, Ok):
                logger.info(f"Connected to {h.result}")
                return HTMXTemplate(
                    template_name="settings/settings_client_uri_ok.html",
                    context={"client": "rtorrent"},
                )
            return HTMXTemplate(template_str=f"<h1>Connection failed: {h!s}</h1>")
    except Exception as e:
        return HTMXTemplate(template_str=f"<h1>Connection failed: {e!s}</h1>")


@get("/deluge_check", name="deluge_check")
async def deluge_check(
    uri: str,
) -> HTMXTemplate:
    """Check if the socket is valid."""
    try:
        url = httpx.URL(uri)
        async with DelugeClient(url) as deluge_client:
            h = await deluge_client.session_info()
            if isinstance(h, Ok):
                logger.info(f"Connected to {h.result}")
                return HTMXTemplate(
                    template_name="settings/settings_client_uri_ok.html",
                    context={"client": "deluge"},
                )
        return HTMXTemplate(template_str=f"<h1>Connection failed: {h!s}</h1>")

    except Exception as e:
        return HTMXTemplate(template_str=f"<h1>Connection failed: {e!s}</h1>")


@get("/signup", name="signup_get")
async def signup_get() -> Template:
    """Render the signup form."""
    return HTMXTemplate(template_name="signup.html")


async def insert_user(
    request: Request,
    data: SignupForm,
    asyncpg_connection: Connection,
) -> Redirect:
    """Insert a new user into the database."""
    password_hash = _get_password_hash(data.password)
    user_id = await asyncpg_connection.fetchval(
        """insert into users(username, password_hash) values ($1 ,$2) returning id""",
        data.username,
        password_hash,
    )
    if user_id:
        await asyncpg_connection.execute(
            """insert into settings(client, uri, uri_type, user_id, base_path, alias) values ($1, $2, $3, $4, $5, $6)""",  # noqa: E501
            data.client.value,
            data.uri,
            data.uri_type,
            user_id,
            data.base_path.as_posix(),
            data.alias,
        )
    request.set_session(
        {"username": data.username, "user_id": user_id},
    )
    return Redirect("/torrents")


@post("/signup", name="signup_post")
async def signup_post(
    request: Request,
    data: Annotated[SignupForm, Body(media_type=RequestEncodingType.URL_ENCODED)],
    asyncpg_connection: Connection,
) -> Redirect | HTTPException:
    """Signup endpoint."""
    logger.info(f"Settings are: {data}")
    match data.client:
        case TorrentClient.rtorrent | TorrentClient.deluge:
            return await insert_user(request, data, asyncpg_connection)
        case TorrentClient.transmission:  # pragma: no cover
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Transmission client is not yet implemented",
            )
        case _:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Bad client")


def _get_password_hash(password: str) -> str:
    """Get the hashed password from the plaintext."""
    return pwd_context.hash(password)


@get("/logout", name="logout")
async def logout(request: Request) -> Redirect:
    """Logout the user."""
    request.session.clear()
    return Redirect("/auth/login")


auth_router = Router(
    route_handlers=[
        logout,
        login_get,
        login_post,
        signup_get,
        signup_post,
        rtorrent_check,
        deluge_check,
    ],
    path="/auth",
)
