"""Home router."""

import asyncio
import logging
from asyncio import Task
from typing import Annotated

import msgspec
from asyncpg import Connection
from httpx import AsyncClient
from jsonrpcclient import Error, Ok
from litestar import Request, Router, get, post
from litestar.contrib.htmx.request import HTMXRequest
from litestar.contrib.htmx.response import HTMXTemplate
from litestar.datastructures import UploadFile
from litestar.enums import RequestEncodingType
from litestar.exceptions import HTTPException
from litestar.params import Body
from litestar.plugins.flash import flash
from litestar.response import Redirect, Template
from litestar.status_codes import HTTP_303_SEE_OTHER, HTTP_400_BAD_REQUEST

from yatg.clients.base import JSONEncoderResponse, TorrentAsyncClient
from yatg.models import (
    AddTorrentFormFile,
    AddTorrentFormURL,
    StartStopTorrentForm,
    TabSetting,
    TagsTorrentForm,
    TorrentContent,
)

logger = logging.getLogger(__name__)


@get("/", name="home_get")
async def home_get() -> Template:
    """Render the home page."""
    return Template(template_name="home.html")


@get("/torrents", name="torrents")
async def torrents(
    request: Request, asyncpg_connection: Connection, limit: int = 20, page: int = 1
) -> HTMXTemplate:
    """Main layout page the has the tabs for each registered client."""
    settings = await asyncpg_connection.fetch(
        """select id, client, uri, uri_type, alias from settings where user_id=(select id from users where username=$1)""",  # noqa: E501
        request.session["username"],
    )
    tabs = msgspec.convert(
        [(s[0], s[1], s[2], s[3], s[4]) for s in settings], type=list[TabSetting]
    )
    return HTMXTemplate(
        template_name="layout.html",
        context={"tabs": tabs, "limit": limit, "page": page},
    )


@get("torrents/{setting_id:int}", name="torrents_main")
async def torrents_main(
    torrent_client: TorrentAsyncClient,
    setting_id: int,
    limit: int,
    page: int,
    # status: str | None,
    # tracker: str | None,
    # tag: str | None,
) -> HTMXTemplate:
    """Render the main page for the selected client."""
    torrents = await torrent_client.get_torrents_list()
    statuses_count, tags_count, trackers_count = torrent_client._get_stats(torrents)
    # filtered_torrents = await _filter_rtorrent_list(
    #     torrents,
    #     status_filter=status,
    #     tracker_filter=tracker,
    #     tag_filter=tag,
    # )
    # paged_torrents = filtered_torrents[
    #     (page - 1) * limit : (page - 1) * limit + limit
    # ]
    return HTMXTemplate(
        template_name="torrent.html",
        context={
            "limit": limit,
            "page": page,
            "client": torrent_client,
            # "torrents": paged_torrents,
            "torrents": torrents,
            "statuses_count": statuses_count,
            "tags_count": tags_count,
            "trackers_count": trackers_count,
            "setting_id": setting_id,
        },
    )
    # match torrent_client:
    #     case DelugeClient():
    #         return HTMXTemplate(
    #             template_name="deluge.html",
    #         )
    #     case RtorrentClient():
    #         torrents = await _get_rtorrent_list(rtorrent_client=torrent_client)
    #         statuses_count, tags_count, trackers_count = _get_stats(torrents)
    #         filtered_torrents = await _filter_rtorrent_list(
    #             torrents,
    #             status_filter=status,
    #             tracker_filter=tracker,
    #             tag_filter=tag,
    #         )
    #         paged_torrents = filtered_torrents[
    #             (page - 1) * limit : (page - 1) * limit + limit
    #         ]
    #         return HTMXTemplate(
    #             template_name="rtorrent.html",
    #             context={
    #                 "limit": limit,
    #                 "page": page,
    #                 "client": torrent_client,
    #                 "torrents": paged_torrents,
    #                 "statuses_count": statuses_count,
    #                 "tags_count": tags_count,
    #                 "trackers_count": trackers_count,
    #                 "setting_id": setting_id,
    #             },
    #         )
    #     case _:
    #         raise HTTPException(status_code=400, detail="Invalid client")
    #


@get("/switch/{setting_id:int}", name="torrent_trigger_switch")
async def torrent_trigger_switch(request: HTMXRequest, setting_id: int) -> HTMXTemplate:
    """Route to determine the modal to show depending on the event triggered."""
    triggering_event = request.htmx.triggering_event
    _hash = request.htmx.trigger
    # match triggering_event["htmx-internal-data"]["triggerSpec"]["trigger"]:
    match triggering_event["type"]:
        case "dblclick":
            logger.debug("dblclick")
            return HTMXTemplate(
                template_name="torrent_info_dialog.html",
                context={"hash": _hash, "setting_id": setting_id},
            )

        case "click":
            logger.debug("click")
            return HTMXTemplate(
                template_name="contextual_menu_dialog.html",
                context={"hash": _hash, "setting_id": setting_id},
            )

        case _:
            logger.error("Should not be possible")
            raise HTTPException(
                detail="Event not handled, raise an issue",
                status_code=HTTP_400_BAD_REQUEST,
            )


@get(path="/info/{setting_id:int}/{_hash:str}/files", name="torrent_info_files")
async def torrent_info_files(
    setting_id: int,  # noqa: ARG001
    _hash: str,
    torrent_client: TorrentAsyncClient,
) -> HTMXTemplate:
    """Get the torrent files content."""
    get_files = await torrent_client.get_info_files(_hash)

    if isinstance(get_files, Ok):
        torrent_content = msgspec.convert(
            get_files.result,
            type=list[TorrentContent],
            strict=False,
        )
    return HTMXTemplate(
        template_name="torrent_info_files.html",
        context={"hash": _hash, "torrent_content": torrent_content},
    )


@get(path="/info/{setting_id:int}/{_hash:str}", name="torrent_info_dialog")
async def torrent_info_dialog(
    setting_id: int,  # noqa: ARG001
    _hash: str,
) -> HTMXTemplate:
    """Get the full torrent info dialog."""
    return HTMXTemplate(
        template_name="torrent_info_dialog.html", context={"hash": _hash}
    )


@get(path="/tags/{_hash:str}", name="torrent_tags_dialog")
async def torrent_tags_dialog(_hash: str) -> HTMXTemplate:
    """Get the tags dialog."""
    return HTMXTemplate(
        template_name="torrent_tags_dialog.html", context={"hash": _hash}
    )


@post(path="/tags/{_hash:str}", name="torrent_tags_dialog_post")
async def torrent_tags_dialog_post(
    request: HTMXRequest,
    torrent_client: TorrentAsyncClient,
    _hash: str,
    data: Annotated[TagsTorrentForm, Body(media_type=RequestEncodingType.URL_ENCODED)],
) -> Redirect:
    """Set the tags."""
    set_tags = await torrent_client.set_tags(
        _hash,
        data.tags,
    )
    if isinstance(set_tags, Error):
        flash(request, f"Failed to set tags: {set_tags.message}", "danger")
    elif isinstance(set_tags, Ok):
        flash(request, f"Tags saved: {data.tags}", "success")
    return Redirect("/torrents", status_code=HTTP_303_SEE_OTHER)


@get("/client_info/{setting_id:int}", name="torrent_client_info")
async def torrent_client_info(torrent_client: TorrentAsyncClient) -> HTMXTemplate:
    """Get the client info for the given setting_id."""
    client_info = await torrent_client.session_info()
    return HTMXTemplate(template_str=f"<h1>{client_info!s}</h1>")


# f4aa391c21afbe594b1f428334c46c2160ba4d46


@post(path="/start/{setting_id:int}", name="torrent_start")
async def torrent_start(
    request: HTMXRequest,
    setting_id: int,  # noqa: ARG001
    torrent_client: TorrentAsyncClient,
    data: Annotated[
        StartStopTorrentForm, Body(media_type=RequestEncodingType.URL_ENCODED)
    ],
) -> Redirect:
    """Start one or more torrents."""
    if isinstance(data.hashes, str):
        data.hashes = [data.hashes]
    for _hash in data.hashes:
        open_, start_ = await torrent_client.start_torrent(_hash)

        if isinstance(open_, Ok):
            flash(request, f"Torrent {_hash} open", "success")
        elif isinstance(open_, Error):
            flash(request, f"Failed to open torrent {_hash}", "danger")

        if isinstance(start_, Ok):
            flash(request, f"Torrent {_hash} started", "success")
        elif isinstance(start_, Error):
            flash(request, f"Failed to start torrent {_hash}", "danger")
    # works only with htmx hx-target body in the element that's calling this
    return Redirect("/torrents", status_code=HTTP_303_SEE_OTHER)


@post(path="/stop/{setting_id:int}", name="torrent_stop")
async def torrent_stop(
    request: HTMXRequest,
    setting_id: int,  # noqa: ARG001
    torrent_client: TorrentAsyncClient,
    data: Annotated[
        StartStopTorrentForm, Body(media_type=RequestEncodingType.URL_ENCODED)
    ],
) -> Redirect:
    """Stop one or more torrents."""
    if isinstance(data.hashes, str):
        data.hashes = [data.hashes]
    for _hash in data.hashes:
        stop, close = await torrent_client.stop_torrent(_hash)
        if isinstance(stop, Ok):
            flash(request, f"Torrent {_hash} stooped", "success")
        elif isinstance(stop, Error):
            flash(request, f"Failed to stop torrent {_hash}", "danger")
        if isinstance(close, Ok):
            flash(request, f"Torrent {_hash} closed", "success")
        elif isinstance(close, Error):
            flash(request, f"Failed to close torrent {_hash}", "danger")
    # works only with htmx hx-target body in the element that's calling this
    return Redirect("/torrents", status_code=HTTP_303_SEE_OTHER)


@post(path="/erase/{setting_id:int}", name="torrent_erase")
async def torrent_erase(
    request: HTMXRequest,
    setting_id: int,  # noqa: ARG001
    torrent_client: TorrentAsyncClient,
    data: Annotated[
        StartStopTorrentForm, Body(media_type=RequestEncodingType.URL_ENCODED)
    ],
) -> Redirect:
    """Erase one or more torrents."""
    if isinstance(data.hashes, str):
        data.hashes = [data.hashes]
    results = []
    for _hash in data.hashes:
        erase = await torrent_client.delete_torrent(_hash)
        results.append((_hash, erase))

    for result in results:
        if isinstance(result[1], Error):
            flash(
                request,
                f"Failed to erase torrent {result[0]}: {result[1].message}",
                "danger",
            )
        elif isinstance(result[1], Ok):
            flash(
                request,
                f"Torrent erased: {result[0]}: {result[1].result}",
                "success",
            )

    # works only with htmx hx-target body in the element that's calling this
    return Redirect("/torrents", status_code=HTTP_303_SEE_OTHER)


@post(path="/check_hash/{setting_id:int}", name="torrent_check_hash")
async def torrent_check_hash(
    setting_id: int,  # noqa: ARG001
    torrent_client: TorrentAsyncClient,
    data: Annotated[
        StartStopTorrentForm, Body(media_type=RequestEncodingType.URL_ENCODED)
    ],
) -> Redirect:
    """Check the hash of one or more torrents."""
    if isinstance(data.hashes, str):
        data.hashes = [data.hashes]
    results = []
    for _hash in data.hashes:
        check = await torrent_client.check_hash(_hash)
        results.append((_hash, check))
    for result in results:
        if isinstance(result[1], Error):
            logger.error(
                f"Failed to check hash for torrent {result[0]}: {result[1].message}"
            )
        elif isinstance(result[1], Ok):
            logger.info(f"Torrent hash checking: {result[0]}: {result[1].result}")
    return Redirect("/torrents", status_code=HTTP_303_SEE_OTHER)


async def add_torrents_by_file(
    data: AddTorrentFormFile | AddTorrentFormURL,
    torrent_client: TorrentAsyncClient,
) -> list[JSONEncoderResponse]:
    """Add torrents by file."""
    return await torrent_client.load_torrent(data)


async def add_torrents(
    data: AddTorrentFormFile | AddTorrentFormURL,
    torrent_client: TorrentAsyncClient,
    tclient: AsyncClient | None,
) -> list[JSONEncoderResponse]:
    """Add torrents depending on the data."""
    if isinstance(data, AddTorrentFormURL):
        data.urls = [data.urls] if isinstance(data.urls, str) else data.urls
        if tclient:
            download_file_tasks: list[Task] = [
                asyncio.create_task(tclient.get(url)) for url in data.urls
            ]
            try:
                files = await asyncio.gather(*download_file_tasks)
            except Exception as e:
                logger.error(str(e))
                raise e
            data.raw_data = [f.content for f in files]
    elif isinstance(data, AddTorrentFormFile):
        data.paths = [data.paths] if isinstance(data.paths, UploadFile) else data.paths
        data.raw_data = [await path.read() for path in data.paths]
    return await add_torrents_by_file(data, torrent_client)


@get(
    path="/add/menu/{setting_id:int}",
    name="torrent_add_menu_dialog",
)
async def torrent_add_menu_dialog(setting_id: int) -> HTMXTemplate:
    """Get the add torrent menu."""
    return HTMXTemplate(
        template_name="torrent_add_menu_dialog.html",
        context={"setting_id": setting_id},
    )


@get(
    path="/add/by_file/{setting_id:int}",
    name="torrent_add_by_file",
)
async def torrent_add_by_file(
    torrent_client: TorrentAsyncClient,
    setting_id: int,
) -> HTMXTemplate:
    """Add a torrent by file or by URL or create one."""
    get_tags = await torrent_client.get_tags()
    if isinstance(get_tags, Error):
        existing_tags = set()
    elif isinstance(get_tags, Ok):
        existing_tags = {t[0] for t in get_tags.result}
    return HTMXTemplate(
        template_name="torrent_add_by_file.html",
        context={"existing_tags": existing_tags, "setting_id": setting_id},
    )


@post(
    path="/add/by_file/{setting_id:int}",
    name="torrent_add_by_file_post",
)
async def torrent_add_by_file_post(
    request: HTMXRequest,
    setting_id: int,  # noqa: ARG001
    torrent_client: TorrentAsyncClient,
    data: Annotated[
        AddTorrentFormFile, Body(media_type=RequestEncodingType.MULTI_PART)
    ],
) -> Redirect:
    """Add a torrent by file."""
    added = await add_torrents(data, torrent_client, None)
    logger.debug(added)
    for add in added:
        if isinstance(add, Error):
            flash(request, f"Failed to add torrent: {add.message}", "danger")
        elif isinstance(add, Ok):
            flash(request, f"Torrent added: {add.result}", "success")
    return Redirect("/torrents", status_code=HTTP_303_SEE_OTHER)


@get(
    path="/add/by_url/{setting_id:int}",
    name="torrent_add_by_url",
)
async def torrent_add_by_url(
    torrent_client: TorrentAsyncClient,
    setting_id: int,
) -> HTMXTemplate:
    """Add a torrent by file or by URL or create one."""
    get_tags = await torrent_client.get_tags()
    if isinstance(get_tags, Error):
        existing_tags = set()
    elif isinstance(get_tags, Ok):
        existing_tags = {t[0] for t in get_tags.result}
    return HTMXTemplate(
        template_name="torrent_add_by_url.html",
        context={"existing_tags": existing_tags, "setting_id": setting_id},
    )


@get(
    path="/add/by_url/{setting_id:int}/item/{next_url_index:int}/{prev_url_index:int}",
    name="torrent_add_by_url_item",
)
async def torrent_add_by_url_item(
    setting_id: int,
    next_url_index: int,
    prev_url_index: int,
) -> HTMXTemplate:
    """Add torrent url input given the next and previous index."""
    logger.debug(f"prev: {prev_url_index}, next: {next_url_index}")
    return HTMXTemplate(
        template_name="torrent_add_by_url_item.html",
        context={
            "setting_id": setting_id,
            "next_url_index": next_url_index,
            "prev_url_index": prev_url_index,
        },
    )


@post(
    path="/add/by_url/{setting_id:int}",
    name="torrent_add_by_url_post",
)
async def torrent_add_by_url_post(
    request: HTMXRequest,
    setting_id: int,  # noqa: ARG001
    torrent_client: TorrentAsyncClient,
    data: Annotated[
        AddTorrentFormURL, Body(media_type=RequestEncodingType.URL_ENCODED)
    ],
    tclient: AsyncClient,
) -> Redirect:
    """Add a torrent by URL."""
    added = await add_torrents(data, torrent_client, tclient)
    logger.debug(added)
    for add in added:
        if isinstance(add, Error):
            flash(request, f"Failed to add torrent: {add.message}", "danger")
        elif isinstance(add, Ok):
            flash(request, f"Torrent added: {add.result}", "success")
    return Redirect("/torrents", status_code=HTTP_303_SEE_OTHER)


@get(path="/info/{setting_id:int}/{_hash:str}/trackers", name="torrent_info_trackers")
async def torrent_info_trackers(
    setting_id: int,  # noqa: ARG001
    _hash: str,
    torrent_client: TorrentAsyncClient,
) -> HTMXTemplate:
    """Get the torrent trackers."""
    trackers = await torrent_client.get_tracker(
        _hash,
    )

    return HTMXTemplate(
        template_name="torrent_info_trackers.html",
        context={"hash": _hash, "trackers": trackers},
    )
    raise HTTPException(detail="okkkk", status_code=HTTP_400_BAD_REQUEST)


@get(path="/info/{setting_id:int}/{_hash:str}/details", name="torrent_info_details")
async def torrent_info_details(
    setting_id: int,  # noqa: ARG001
    torrent_client: TorrentAsyncClient,
    _hash: str,
) -> HTMXTemplate:
    """Get the torrent details."""
    torrents = await torrent_client.get_torrents_list(_hash=_hash)
    return HTMXTemplate(
        template_name="torrent_info_details.html",
        context={"hash": _hash, "torrent": torrents[0]},
    )


@get(path="/info/{setting_id:int}/{_hash:str}/peers", name="torrent_info_peers")
async def torrent_info_peers(
    setting_id: int,  # noqa: ARG001
    _hash: str,
    torrent_client: TorrentAsyncClient,
) -> HTMXTemplate:
    """Get the torrent peers."""
    get_peers = await torrent_client.get_peers(_hash=_hash)
    return HTMXTemplate(
        template_name="torrent_info_peers.html",
        context={"hash": _hash, "peers": get_peers},
    )


home_router = Router(
    route_handlers=[
        home_get,
        torrents,
        torrents_main,
        torrent_client_info,
        torrent_add_menu_dialog,
        torrent_add_by_file,
        torrent_add_by_file_post,
        torrent_add_by_url,
        torrent_add_by_url_item,
        torrent_add_by_url_post,
        torrent_start,
        torrent_stop,
        torrent_erase,
        torrent_check_hash,
        torrent_trigger_switch,
        torrent_tags_dialog,
        torrent_tags_dialog_post,
        torrent_info_trackers,
        torrent_info_details,
        torrent_info_dialog,
        torrent_info_files,
        torrent_info_peers,
    ],
    path="/",
)
