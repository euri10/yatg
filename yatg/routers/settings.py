"""Settings router."""

import logging
from pathlib import Path
from typing import Annotated

import msgspec
from asyncpg import Connection, UniqueViolationError
from litestar import Request, Router, delete, get, post
from litestar.contrib.htmx.request import HTMXRequest
from litestar.contrib.htmx.response import HTMXTemplate
from litestar.enums import RequestEncodingType
from litestar.exceptions import HTTPException
from litestar.params import Body
from litestar.plugins.flash import flash
from litestar.response import Redirect

from yatg.models import (
    DelugeConnection,
    RtorrentConnection,
    SettingsForm,
    TabSetting,
    TorrentClient,
)
from yatg.utils import searching_all_directories

logger = logging.getLogger(__name__)


@get("/", name="settings_get")
async def settings_get() -> HTMXTemplate:
    """Render the settings page."""
    return HTMXTemplate(template_name="settings/settings.html")


@delete("/{tab_alias:str}", name="settings_delete", status_code=200)
async def settings_delete(
    request: HTMXRequest,
    asyncpg_connection: Connection,
    tab_alias: str,
    limit: int = 20,
    page: int = 1,
) -> HTMXTemplate:
    """Delete a settings entry."""
    result = await asyncpg_connection.execute(
        """delete from settings where alias = $1
        and user_id = (select id from users where username = $2)""",
        tab_alias,
        request.session["username"],
    )
    flash_message = "Settings deleted" if result == "DELETE 1" else "Settings not found"
    flash(request=request, message=flash_message, category="success")
    settings = await asyncpg_connection.fetch(
        """select id, client, uri, uri_type, alias from settings
        where user_id=(select id from users where username=$1)""",
        request.session["username"],
    )
    tabs = msgspec.convert(
        [(s[0], s[1], s[2], s[3], s[4]) for s in settings], type=list[TabSetting]
    )
    return HTMXTemplate(
        template_name="layout.html",
        context={"tabs": tabs, "limit": limit, "page": page},
    )


@post("/", name="settings_post")
async def settings_post(
    request: Request,
    data: Annotated[SettingsForm, Body(media_type=RequestEncodingType.URL_ENCODED)],
    asyncpg_connection: Connection,
) -> Redirect:
    """Save the settings."""
    logger.info(f"Settings are: {data}")
    match data.client:
        case TorrentClient.rtorrent | TorrentClient.deluge:
            user_id = await asyncpg_connection.fetchval(
                """select id from users where username = $1""",
                request.session["username"],
            )
            if user_id:
                try:
                    await asyncpg_connection.execute(
                        """insert into settings(client, uri, uri_type, user_id, base_path, alias) values ($1, $2, $3, $4, $5, $6)""",  # noqa: E501
                        data.client.value,
                        data.uri,
                        data.uri_type,
                        user_id,
                        data.base_path.as_posix(),
                        data.alias,
                    )
                except UniqueViolationError:
                    flash(
                        request=request,
                        message="Settings already exists",
                        category="danger",
                    )
                    return Redirect("/settings")
            return Redirect("/torrents")
        case TorrentClient.transmission:
            raise HTTPException(
                status_code=400, detail="Transmission client is not yet implemented"
            )


@get("/client", name="settings_client_get")
async def settings_client_get(client: TorrentClient) -> HTMXTemplate:
    """Render the settings client info needed."""
    match client.value:
        case TorrentClient.rtorrent:
            return HTMXTemplate(template_name="settings/settings_client_rtorrent.html")
        case TorrentClient.deluge:
            return HTMXTemplate(template_name="settings/settings_client_deluge.html")
        case TorrentClient.transmission:
            return HTMXTemplate(
                template_str="Transmission client is not yet implemented"
            )
        case _:
            raise HTTPException(status_code=400, detail="Invalid client type")


@get("/client/rtorrent", name="settings_client_rtorrent")
async def settings_client_rtorrent(uri_type: RtorrentConnection) -> HTMXTemplate:
    """Render the settings client rtorrent info needed."""
    logger.debug(f"Connection type: {uri_type}")
    match uri_type:
        case RtorrentConnection.socket:
            uri_placeholder = "scgi+unix:///config/.local/share/rtorrent/rtorrent.sock"
        case RtorrentConnection.tcp:
            uri_placeholder = "scgi://localhost:50005"
        case _:
            raise HTTPException(status_code=400, detail="Invalid connection type")
    return HTMXTemplate(
        template_name="settings/settings_client_rtorrent_uri.html",
        context={
            "uri_placeholder": uri_placeholder,
            "rtorrent_connection": uri_type.value,
        },
    )


@get("/client/deluge", name="settings_client_deluge")
async def settings_client_deluge(uri_type: DelugeConnection) -> HTMXTemplate:
    """Render the settings client deluge info needed."""
    logger.debug(f"Connection type: {uri_type}")
    match uri_type:
        case DelugeConnection.tcp:
            uri_placeholder = "http://localclient:359cd030a589a35b956bc90329884b73f3ea2e10@localhost:58846"
    return HTMXTemplate(
        template_name="settings/settings_client_deluge_uri.html",
        context={
            "uri_placeholder": uri_placeholder,
            "rtorrent_connection": uri_type.value,
        },
    )


@get("/dirs", name="dir_listing")
async def dir_listing(
    base_path: Path | None, destination: Path | None, parent: Path | None
) -> HTMXTemplate:
    """List all directories and files in the given path."""
    if base_path and not destination and not parent:
        dirs, files, error = await searching_all_directories(base_path)
        destination = base_path
        parent = base_path.parent
    elif not base_path and destination and not parent:
        dirs, files, error = await searching_all_directories(destination)
        base_path = destination
        parent = destination.parent
    elif not base_path and not destination and parent:
        dirs, files, error = await searching_all_directories(parent)
        base_path = parent
        parent = parent.parent
    else:
        raise HTTPException(status_code=400, detail="Invalid query parameters")

    return HTMXTemplate(
        template_name="files.html",
        context={
            "dirs": dirs,
            "files": files,
            "base_path": base_path,
            "destination": destination,
            "parent": parent,
            "error": error,
        },
    )


@get("/tab_edit/{tab_alias:str}", name="settings_tab_edit")
async def settings_tab_edit(tab_alias: str) -> HTMXTemplate:
    """Edit the alias of a tab."""
    return HTMXTemplate(
        template_name="settings/settings_tab_dialog.html",
        context={"tab_alias": tab_alias},
    )


class TabEditForm(msgspec.Struct):
    """Tab edit form."""

    tab_alias: str


@post("/tab_edit/{tab_alias:str}", name="settings_tab_edit_post")
async def settings_tab_edit_post(
    request: HTMXRequest,
    tab_alias: str,
    data: Annotated[TabEditForm, Body(media_type=RequestEncodingType.URL_ENCODED)],
    asyncpg_connection: Connection,
    limit: int = 20,
    page: int = 1,
) -> HTMXTemplate:
    """Edit the alias of a tab."""
    changed_tab_alias = await asyncpg_connection.execute(
        """update settings set alias=$1 where alias=$2""", data.tab_alias, tab_alias
    )
    if changed_tab_alias == "UPDATE 1":
        flash(request, "Tab update succeeded", "success")
    else:
        flash(request, "Tab update failed", "danger")

    settings = await asyncpg_connection.fetch(
        """select id, client, uri, uri_type, alias from settings
        where user_id=(select id from users where username=$1)""",
        request.session["username"],
    )
    tabs = msgspec.convert(
        [(s[0], s[1], s[2], s[3], s[4]) for s in settings], type=list[TabSetting]
    )
    return HTMXTemplate(
        template_name="layout.html",
        context={"tabs": tabs, "limit": limit, "page": page},
    )


settings_router = Router(
    path="/settings",
    route_handlers=[
        settings_get,
        settings_post,
        settings_delete,
        settings_client_get,
        settings_client_rtorrent,
        settings_client_deluge,
        dir_listing,
        settings_tab_edit,
        settings_tab_edit_post,
    ],
)
