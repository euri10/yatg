"""rTorrent router."""

import logging
import re
from pathlib import Path
from typing import Annotated

from jsonrpcclient import Error, Ok
from litestar import Router, get, post, websocket_listener
from litestar.contrib.htmx.request import HTMXRequest
from litestar.contrib.htmx.response import HTMXTemplate
from litestar.enums import RequestEncodingType
from litestar.exceptions import HTTPException
from litestar.params import Body
from litestar.plugins.flash import flash
from litestar.response import (
    File,
    Redirect,
)
from litestar.status_codes import (
    HTTP_303_SEE_OTHER,
    HTTP_401_UNAUTHORIZED,
)

from yatg.clients.rtorrent import RtorrentClient
from yatg.models import (
    SearchForm,
)

logger = logging.getLogger(__name__)


@post("/methods", name="rtorrent_methods")
async def rtorrent_methods(
    data: Annotated[SearchForm, Body(media_type=RequestEncodingType.URL_ENCODED)],
    rtorrent_client: RtorrentClient,
) -> HTMXTemplate:
    """Get the list of methods available in rtorrent."""
    v = await rtorrent_client.send_daemon(
        "system.listMethods",
        ("",),
    )
    methods: list[str] = []
    if isinstance(v, Ok):
        if data.search_type == "string":
            methods = [x for x in v.result if x.find(data.s) >= 0]
        elif data.search_type == "regex":
            for method in v.result:
                pattern = re.compile(data.s)
                m = re.match(pattern, method)
                if m:
                    methods.append(method)
    return HTMXTemplate(
        template_name="rtorrent_methods.html",
        context={"methods": methods, "method_l": len(methods)},
    )


@websocket_listener("/wsd", name="wsd")
async def wsd(data: str) -> str:
    """Websocket listener to remove just for debugging purposes."""
    return data


# @websocket("/rtorrent_ws", name="rtorrent_ws")
# async def rtorrent_ws(
#     rtorrent_client: RtorrentClient, socket: WebSocket, limit: int, page: int
# ) -> None:
#     """Websocket handler for rtorrent."""
#     logger.info(f"WEBSOCKET: {page} {limit}")
#     await socket.accept()
#     status_filter_set = None
#     tag_filter_set = None
#     tracker_filter_set = None
#     hashes_set = []
#     # torrents_old = await get_rtorrent_list(
#     #     rtorrent_client=rtorrent_client,
#     # )
#     try:
#         while True:
#             try:
#                 data = await asyncio.wait_for(socket.receive_text(), timeout=5)
#             except Exception as e:
#                 data = None
#                 logger.error(f"no data received: {e}")
#             if data:
#                 jdata = json.loads(data)
#                 status_filter_set = jdata.get("status_filter")
#                 tag_filter_set = jdata.get("tag_filter")
#                 tracker_filter_set = jdata.get("tracker_filter")
#                 hashes_set = jdata.get("hashes")
#                 if not isinstance(hashes_set, list):
#                     hashes_set = [hashes_set]
#             status_filter = status_filter_set
#             tag_filter = tag_filter_set
#             tracker_filter = tracker_filter_set
#             hashes = hashes_set
#
#             torrents = await get_rtorrent_list(
#                 rtorrent_client=rtorrent_client,
#             )
#             # if torrents_old != torrents:
#             #     logger.info(
#             #         f"DIFF: {[old_t == t for old_t, t in zip(torrents_old, torrents)]}"  # noqa: E501
#             #     )
#             logger.info(f"status_filter: {status_filter}, tag_filter: {tag_filter}")
#             logger.info(f"hashes: {hashes}")
#             filtered_torrents = await _filter_rtorrent_list(
#                 torrents,
#                 status_filter=status_filter,
#                 tracker_filter=tracker_filter,
#                 tag_filter=tag_filter,
#             )
#             paged_torrents = filtered_torrents[
#                 (page - 1) * limit : (page - 1) * limit + limit
#             ]
#             env: Environment = socket.app.template_engine.engine
#             # fill #torrent_list
#             list_html = env.get_template("rtorrent_list.html").render(
#                 request=socket,
#                 torrents=paged_torrents,
#                 selected_hashes=hashes,
#             )
#             await socket.send_text(
#                 f"""<div id="torrent_list" class="col-span-9">{list_html}</div>"""
#             )
#             # fill #status
#             statuses_count, tags_count, trackers_count = _get_stats(torrents)
#             status_html = env.get_template("rtorrent_status_count.html").render(
#                 request=socket,
#                 statuses_count=statuses_count,
#                 tags_count=tags_count,
#                 trackers_count=trackers_count,
#             )
#             await socket.send_text(
#                 f"""<div id="status" class="col-span-1">{status_html}</div>"""
#             )
#             # fill #pagination
#             logger.info(f"page: {page}, limit: {limit}")
#             pagination_html = env.get_template("torrents_pagination.html").render(
#                 request=socket,
#                 limit=limit,
#                 page=page,
#                 torrents=paged_torrents,
#                 client="rtorrent",
#             )
#             await socket.send_text(f"""<div id="pagination">{pagination_html}</div>""")  # noqa: E501
#
#     except WebSocketDisconnect as wse:
#         logger.error(wse)
#         raise wse
#     except ClientDisconnected as e:
#         logger.error(str(e))
#         await socket.close()
#     finally:
#         logger.info(f"FINALLY {page} {limit}")


@get(path="/download/{_hash:str}", name="rtorrent_download_torrent")
async def rtorrent_download_torrent(
    rtorrent_client: RtorrentClient, _hash: str
) -> File:
    """Download the .torrent file."""
    session = await rtorrent_client.send_daemon(
        method="session.path",
        params=(_hash,),
    )
    if isinstance(session, Ok):
        # download the torrent file from that session direcory
        torrent_file = Path(session.result) / f"{_hash}.torrent"
        return File(torrent_file)
    raise HTTPException(
        detail="Can't download .torrent file",
        status_code=HTTP_401_UNAUTHORIZED,
    )


@get(path="/reannounce/{_hash:str}", name="rtorrent_reannounce_torrent")
async def rtorrent_reannounce_torrent(
    request: HTMXRequest, rtorrent_client: RtorrentClient, _hash: str
) -> Redirect:
    """Reannounce the torrent."""
    reannounce = await rtorrent_client.send_daemon(
        method="d.tracker_announce",
        params=(_hash,),
    )
    if isinstance(reannounce, Ok):
        flash(request, f"Torrent {_hash} reannounced", "success")
    elif isinstance(reannounce, Error):
        flash(request, f"Failed to reannounce torrent {_hash}", "danger")

    return Redirect("/torrents", status_code=HTTP_303_SEE_OTHER)


rtorrent_router = Router(
    path="/rtorrent",
    route_handlers=[
        rtorrent_methods,
        wsd,
        rtorrent_download_torrent,
        rtorrent_reannounce_torrent,
    ],
)
