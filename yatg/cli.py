"""CLI for yatg."""

from __future__ import annotations

import asyncio
import logging
from dataclasses import dataclass
from pathlib import Path
from typing import Annotated, Literal

import aiofiles
import cappa
from asyncpg import create_pool
from asyncpg_trek import Direction, execute, plan
from asyncpg_trek.asyncpg import AsyncpgBackend
from cappa import Dep, Exit

from yatg.settings import AppSettings

logger = logging.getLogger(__name__)


@dataclass
class Config:
    """Config object for the CLI."""

    app_settings: AppSettings
    migrations_dir: Path


def get_config(yatg: Yatg) -> Config:  # noqa: ARG001  # pragma: no cover
    """CLI config dependency."""
    _migrations_dir = Path(__file__).parent.parent / "migrations"
    if not _migrations_dir.exists():
        raise Exit("No migrations directory", code=1)
    _settings = AppSettings.from_env()
    return Config(app_settings=_settings, migrations_dir=_migrations_dir)


async def migrate(migrate: Migrate, config: Annotated[Config, Dep(get_config)]) -> None:
    """Migrate database logic."""
    if migrate.direction == "up":
        direction = Direction.up
        if migrate.revision:
            revision = migrate.revision  # pragma: no cover
        else:
            async with aiofiles.open(
                Path(config.migrations_dir / "_LATEST_REVISION")
            ) as f:
                contents = await f.read()
                revision = contents.strip()
    elif migrate.direction == "down":  # pragma: no cover
        direction = Direction.down
        if migrate.revision:
            revision = migrate.revision
        else:
            revision = "initial"
    try:
        async with create_pool(
            config.app_settings.postgres_url, min_size=1, max_size=1
        ) as pool:
            async with pool.acquire() as conn:
                backend = AsyncpgBackend(conn)
                async with backend.connect():
                    planned = await plan(
                        backend,
                        config.migrations_dir,
                        target_revision=revision,
                        direction=direction,
                    )
                    await execute(backend, planned)
    except Exception as e:  # pragma: no cover
        raise cappa.Exit(message=f"Error migrating: {e!s}", code=1) from e


@cappa.command(invoke=migrate)
@dataclass
class Migrate:
    """Migrate command."""

    direction: Annotated[
        Literal["up", "down"] | None, cappa.Arg(long=True, default="up")
    ]
    revision: Annotated[str | None, cappa.Arg(long=True, default=None)]


@dataclass
class Yatg:
    """Defines the Yatg commands."""

    command: cappa.Subcommands[Migrate]


def main() -> None:
    """Main for the CLI."""
    asyncio.run(  # pragma: no cover
        cappa.invoke_async(
            Yatg,
        )
    )


if __name__ == "__main__":
    main()  # pragma: no cover
