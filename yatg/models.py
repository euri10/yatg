"""Models for the application."""

import datetime
import typing
from dataclasses import dataclass
from enum import StrEnum, auto
from pathlib import Path

import msgspec
from litestar.datastructures import UploadFile


class TabSetting(msgspec.Struct, array_like=True):
    """Tab setting."""

    id: int
    client: typing.Literal["rtorrent", "deluge", "transmission"]
    uri: str
    uri_type: str
    alias: str


class TrackerInfo(msgspec.Struct, array_like=True):
    """Tracker information."""

    url: str
    is_enabled: bool | None = None
    type: int | None = None
    scrape_complete: int | None = None
    scrape_incomplete: int | None = None


class TorrentInfo(msgspec.Struct, array_like=True, eq=True):
    """Torrent information."""

    hash: str
    name: str
    message: str
    state: int
    custom2: str
    is_active: bool
    is_complete: bool
    is_private: bool
    connection_seed: str
    down_sequential: int
    is_open: bool
    is_hashing: bool
    up_rate: int
    up_total: int
    down_rate: int
    down_total: int
    ratio: float
    bytes_done: int
    size_bytes: int
    directory: str
    custom: datetime.datetime  # addtime
    # custom: str  # addtime
    creation_date: datetime.datetime
    finished_date: datetime.datetime
    timestamp_last_active: datetime.datetime
    custom1: str  # tags
    peers_complete: int
    peers_accounted: int
    trackers: list[TrackerInfo] | None = None
    statuses: list[str] | None = None
    checked: bool = False


class TorrentStatus(StrEnum):
    """Torrent status enumeration."""

    ACTIVE = "active"
    CHECKING = "checking"
    COMPLETE = "complete"
    DOWNLOADING = "downloading"
    ERROR = "error"
    INACTIVE = "inactive"
    SEEDING = "seeding"
    STOPPED = "stopped"


STATUS_ICONS = {
    TorrentStatus.ACTIVE: "fa-sun",
    TorrentStatus.CHECKING: "fa-question",
    TorrentStatus.COMPLETE: "fa-check",
    TorrentStatus.DOWNLOADING: "fa-download",
    TorrentStatus.ERROR: "fa-bomb",
    TorrentStatus.INACTIVE: "fa-moon",
    TorrentStatus.SEEDING: "fa-seedling",
    TorrentStatus.STOPPED: "fa-stop",
}


@dataclass
class RtorrentConfig:
    """Configuration for rtorrent."""

    socket: Path | None = None


class TorrentContent(msgspec.Struct, array_like=True):
    """Torrent content information."""

    path: str
    priority: int
    size_bytes: int
    size_chunks: int
    completed_chunks: int


class TorrentPeersInfo(msgspec.Struct, array_like=True):
    """Torrent peers information."""

    address: str
    client_version: str
    completed_percent: int
    down_rate: int
    up_rate: int
    is_encrypted: bool
    is_incoming: bool


class TorrentClient(StrEnum):
    """Torrent client enumeration."""

    rtorrent = auto()
    deluge = auto()
    transmission = auto()


class SettingsForm(msgspec.Struct):
    """Settings form data."""

    base_path: Path
    client: TorrentClient
    uri_type: str
    uri: str
    alias: str


class SignupForm(SettingsForm):
    """Signup form data."""

    username: str
    password: str


class RtorrentConnection(StrEnum):
    """RTorrent connection enumeration."""

    socket = auto()
    tcp = auto()


class DelugeConnection(StrEnum):
    """Deluge connection enumaration."""

    tcp = auto()


class User(msgspec.Struct):
    """User information."""

    user_id: int
    username: str
    tabs: list[TabSetting] | None = None


class TagsTorrentForm(msgspec.Struct):
    """Form to set the tags."""

    tags: str


class AddTorrentFormFile(msgspec.Struct):
    """Form to add a torrent."""

    base_path: str
    paths: list[UploadFile]
    start: bool = False
    tags: list[str] = []
    is_base_path: bool = False
    is_sequential: bool = False
    is_initial_seeding: bool = False
    raw_data: list[bytes] | None = None


class AddTorrentFormURL(msgspec.Struct):
    """Form to add a torrent by URL."""

    base_path: str
    urls: list[str] | str
    start: bool = False
    tags: list[str] = []
    is_base_path: bool = False
    is_sequential: bool = False
    is_initial_seeding: bool = False
    raw_data: list[bytes] | None = None


class StartStopTorrentForm(msgspec.Struct):
    """Form to start or stop a torrent."""

    hashes: list[str] | str


class SearchForm(msgspec.Struct):
    """Search form."""

    s: str
    search_type: typing.Literal["string", "regex"]


class TorrentInfoDeluge(msgspec.Struct):
    """Torrent information from the deluge RPC client."""

    bytes_done: int
    comment: str
    date_active: int
    date_added: int
    creation_date: datetime.datetime
    date_finished: datetime.datetime
    directory: str
    down_rate: int
    down_total: int
    eta: int
    hash: str
    is_private: bool
    is_initial_seeding: bool
    is_sequential: bool
    message: str
    name: str
    peers_connected: int
    peers_total: int
    percent_complete: float
    priority: int
    ratio: float
    seeds_connected: int
    seeds_total: int
    size_bytes: int
    tags: list[str]
    up_rate: int
    up_total: int
    statuses: list[str] | None = None
    trackers: list[TrackerInfo] | None = None
