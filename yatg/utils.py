"""Utility functions for the application."""

from pathlib import Path
from typing import Any, Type
from urllib.parse import urlparse

from yatg.models import TorrentInfo


def duration_format(seconds: float) -> str:
    """Format a duration in seconds to a human-readable string."""
    # Define constants for time conversions
    units = [
        ("month", 30 * 24 * 60 * 60),
        ("day", 24 * 60 * 60),
        ("hour", 60 * 60),
        ("minute", 60),
        ("second", 1),
    ]

    # Initialize the duration components list
    duration_components = []

    # Calculate the components of the duration
    for unit_name, unit_seconds in units:
        value = int(seconds // unit_seconds)
        if value:
            duration_components.append(f"{value} {unit_name}{'s' if value > 1 else ''}")
        seconds %= unit_seconds

    # Construct the formatted string
    if not duration_components:
        return "0 seconds"
    elif len(duration_components) == 1:  # noqa: RET505
        return duration_components[0]
    else:
        return ", ".join(duration_components[:-1]) + f" and {duration_components[-1]}"


async def _filter_rtorrent_list(
    torrents: list[TorrentInfo],
    status_filter: str | None,
    tracker_filter: str | None,
    tag_filter: str | None,
) -> list[TorrentInfo]:
    if status_filter:
        torrents = [t for t in torrents if status_filter in t.statuses]  # type: ignore[operator]
    if tracker_filter:
        torrents = [
            t
            for t in torrents
            if tracker_filter in [urlparse(tr.url).netloc for tr in t.trackers]  # type: ignore[union-attr]
        ]
    if tag_filter:
        torrents = [t for t in torrents if tag_filter in t.custom1.split(",")]
    return torrents


def dec_hook(_type: Type, obj: Any) -> Any:
    """Custom decoder hook for msgspec."""
    # `type` here is the value of the custom type annotation being decoded.
    if _type is Path:
        # Convert ``obj`` (which should be a ``tuple``) to a complex
        return Path(obj)
    # Raise a NotImplementedError for other types
    raise NotImplementedError(f"Objects of type {_type} are not supported")


async def searching_all_directories(
    directory: Path,
) -> tuple[list[Path], list[Path], str | None]:
    """Search all directories and files in the given path."""
    try:
        dirs = [d for d in directory.iterdir() if d.is_dir()]
        files = [f for f in directory.iterdir() if f.is_file()]
        error = None
    except PermissionError:
        dirs, files = [], []
        error = "Permission denied"
    except FileNotFoundError:
        dirs, files = [], []
        error = "File not found"
    return dirs, files, error
