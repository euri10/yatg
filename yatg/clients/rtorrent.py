"""A json-rpc client for rtorrent using httpx."""

import asyncio
import base64
import datetime
import io
import json
import logging
import time
import typing

import jsonrpcclient
import msgspec.json
from anyio import EndOfStream, connect_tcp, connect_unix
from httpx import URL, AsyncBaseTransport, Request, Response
from jsonrpcclient import Ok

from yatg.clients.base import JSONEncoderResponse, TorrentAsyncClient
from yatg.models import (
    AddTorrentFormFile,
    AddTorrentFormURL,
    TorrentInfo,
    TorrentPeersInfo,
    TorrentStatus,
    TrackerInfo,
)

logger = logging.getLogger(__name__)


class RtorrentClientError(Exception):
    """Exception for httpx-based rtorrent json-rpc client."""


class RtorrentClient(TorrentAsyncClient):
    """A json-rcp capable rtorrent client using a custom httpx transport."""

    def __init__(self, uri: URL):
        """Initialize the rtorrent client."""
        self.uri = uri
        self.transport = RtorrentTransport(uri)
        super().__init__(transport=self.transport, base_url=f"{self.uri.scheme}://")

    async def session_info(self) -> JSONEncoderResponse:
        """Get the session info."""
        return await self.send_daemon(method="session.name", params=("",))

    async def get_tags(self) -> JSONEncoderResponse:
        """Get the tags."""
        return await self.send_daemon(
            method="d.multicall2",
            params=(
                "",
                "main",
                "d.custom1=",
            ),
        )

    async def get_peers(self, _hash: str | None = None) -> list[TorrentPeersInfo]:
        """Get peers."""
        get_peers = await self.send_daemon(
            method="p.multicall",
            params=(
                _hash,
                "",
                "p.address=",
                "p.client_version='",
                "p.completed_percent=",
                "p.down_rate=",
                "p.up_rate=",
                "p.is_encrypted=",
                "p.is_incoming=",
            ),
        )
        if isinstance(get_peers, Ok):
            return msgspec.convert(
                get_peers.result,
                type=list[TorrentPeersInfo],
                strict=False,
            )
        raise RtorrentClientError("Cant convert to TorrentPeersInfo from client")

    async def get_trackers(
        self,
        _hash: str,
    ) -> JSONEncoderResponse:
        """Get the trackers for a given hash."""
        return await self.send_daemon(
            method="t.multicall",
            params=(
                _hash,
                "",
                "t.url=",
                "t.is_enabled=",
                "t.type=",
                "t.scrape_complete=",
                "t.scrape_incomplete=",
            ),
        )

    async def load_torrent(
        self,
        data: AddTorrentFormFile | AddTorrentFormURL,
    ) -> list[JSONEncoderResponse]:
        """Load a torrent."""
        results = []
        if data.raw_data:
            for raw_data in data.raw_data:
                if data.start:
                    start_now_or_later = "start"
                    method_used = "load.start_throw"
                else:
                    start_now_or_later = "load"
                    method_used = "load.throw"
                data_dir = f"/config/.local/share/rtorrent/watch/{start_now_or_later}"
                d = f"d.directory_base.set='{data_dir}'"
                # Call the function with the parameters as a dictionary
                additional_calls = self._get_add_rtorrent_properties_calls(
                    data.base_path,
                    data.is_base_path,
                    data.is_sequential,
                    data.is_initial_seeding,
                    data.tags,
                )

                v = await self.send_daemon(
                    method=method_used,
                    params=(
                        "",
                        "data:base64," + base64.b64encode(raw_data).decode("ascii"),
                        d,
                        *additional_calls,
                    ),
                )
                results.append(v)
        return results

    async def start_torrent(
        self, _hash: str
    ) -> tuple[
        JSONEncoderResponse,
        JSONEncoderResponse,
    ]:
        """Start a torrent."""
        open_ = await self.send_daemon(
            method="d.open",
            params=(_hash,),
        )
        start_ = await self.send_daemon(
            method="d.start",
            params=(_hash,),
        )
        return open_, start_

    async def stop_torrent(
        self, _hash: str
    ) -> tuple[
        JSONEncoderResponse,
        JSONEncoderResponse,
    ]:
        """Stop a torrent."""
        stop = await self.send_daemon(method="d.stop", params=(_hash,))
        close = await self.send_daemon(method="d.close", params=(_hash,))
        return stop, close

    async def delete_torrent(self, _hash: str) -> JSONEncoderResponse:
        """Delete a torrent."""
        return await self.send_daemon(method="d.erase", params=(_hash,))

    async def check_hash(self, _hash: str) -> JSONEncoderResponse:
        """Check a given hash."""
        return await self.send_daemon(method="d.check_hash", params=(_hash,))

    async def get_tracker(
        self,
        _hash: str,
    ) -> list[TrackerInfo]:
        """Get the list of TrackerInfo."""
        get_trackers = await self.send_daemon(
            method="t.multicall",
            params=(
                _hash,
                "",
                "t.url=",
                "t.is_enabled=",
                "t.type=",
                "t.scrape_complete=",
                "t.scrape_incomplete=",
            ),
        )
        if isinstance(get_trackers, Ok):
            return msgspec.convert(
                get_trackers.result,
                type=list[TrackerInfo],
                strict=False,
            )
        raise RtorrentClientError("Cant convert to TrackerInfo fro client")

    async def set_tags(self, _hash: str, tags: str) -> JSONEncoderResponse:
        """Set the tags."""
        return await self.send_daemon(
            method="d.custom1.set",
            params=(_hash, tags),
        )

    def _get_add_rtorrent_properties_calls(
        self,
        destination: str,
        is_base_path: bool,
        is_sequential: bool,
        is_initial_seeding: bool,
        tags: list[str],
    ) -> list[str]:
        """Get the properties to set when adding a torrent."""
        result = [
            'd.tied_to_file.set=""',
            f"d.custom.set=addtime,{round(time.time())}",
            f'{"d.directory_base.set" if is_base_path else "d.directory.set"}="{destination}"',  # noqa: E501
        ]

        if tags:
            result.append(f'd.custom1.set="{",".join(tags)}"')

        if is_sequential:
            result.append("d.down.sequential.set=1")

        if is_initial_seeding:
            result.append("d.connection_seed.set=initial_seed")

        return result

    async def get_info_files(self, _hash: str) -> JSONEncoderResponse:
        """Get the file info in rtorrent client."""
        return await self.send_daemon(
            method="f.multicall",
            params=(
                _hash,
                "",
                "f.path=",
                "f.priority=",
                "f.size_bytes=",
                "f.size_chunks=",
                "f.completed_chunks=",
            ),
        )

    async def get_torrents_list(self, _hash: str | None = None) -> list[TorrentInfo]:
        """Get the list of torrents and filter the answer."""
        params = (
            "",
            "main",
            "d.hash=" if not _hash else f"d.hash={_hash}",
            "d.name=",
            "d.message=",
            "d.state=",
            "d.custom2=",
            "d.is_active=",
            "d.complete=",
            "d.is_private=",
            "d.connection_seed=",
            "d.down.sequential=",
            "d.is_open=",
            "d.hashing=",
            "d.up.rate=",
            "d.up.total=",
            "d.down.rate=",
            "d.down.total=",
            "d.ratio=",
            "d.bytes_done=",
            "d.size_bytes=",
            "d.directory=",
            "d.custom=addtime",
            "d.creation_date=",
            "d.timestamp.finished=",
            "d.timestamp.last_active=",
            "d.custom1=",
            "d.peers_complete=",
            "d.peers_accounted=",
        )
        h = await self.send_daemon(
            method="d.multicall2",
            params=params,
        )
        if isinstance(h, Ok):
            torrents: list[TorrentInfo] = msgspec.convert(
                h.result,
                type=list[TorrentInfo],
                strict=False,
                builtin_types=(datetime.datetime),  # type: ignore[call-overload]
            )
            tasks = []
            for t in torrents:
                task = asyncio.create_task(self.get_tracker(t.hash))
                tasks.append(task)
            trackers = await asyncio.gather(*tasks)
            for t, tracker in zip(torrents, trackers):
                t.trackers = tracker
                statuses = self._get_rtorrent_status_from_properties(
                    is_hashing=t.is_hashing,
                    is_complete=t.is_complete,
                    is_open=t.is_open,
                    up_rate=t.up_rate,
                    down_rate=t.down_rate,
                    state=t.state,
                    message=t.message,
                )
                t.statuses = statuses
        else:
            torrents = []
        return torrents

    def _get_rtorrent_status_from_properties(
        self,
        is_hashing: bool,
        is_complete: bool,
        is_open: bool,
        up_rate: int,
        down_rate: int,
        state: int,
        message: str,
    ) -> list[str]:
        """Get the status of a torrent based on its properties."""
        torrent_status: list[str] = []

        if is_hashing:
            torrent_status.append(TorrentStatus.CHECKING)

        elif is_complete and is_open and state:
            torrent_status.append(TorrentStatus.COMPLETE)
            torrent_status.append(TorrentStatus.SEEDING)

        elif is_complete and is_open and not state:
            torrent_status.append(TorrentStatus.STOPPED)

        elif is_complete and not is_open:
            torrent_status.append(TorrentStatus.STOPPED)
            torrent_status.append(TorrentStatus.COMPLETE)

        elif not is_complete and is_open and state:
            torrent_status.append(TorrentStatus.DOWNLOADING)

        elif not is_complete and is_open and not state:
            torrent_status.append(TorrentStatus.STOPPED)

        elif not is_complete and not is_open:
            torrent_status.append(TorrentStatus.STOPPED)

        if message and isinstance(message, str):
            torrent_status.append(TorrentStatus.ERROR)

        if up_rate or down_rate:
            torrent_status.append(TorrentStatus.ACTIVE)

        else:
            torrent_status.append(TorrentStatus.INACTIVE)

        return torrent_status

    async def send_daemon(
        self, method: str, params: tuple[str | None, ...]
    ) -> JSONEncoderResponse:
        """Generic method to send a json-rpc call to the rtorrent client."""
        try:
            v = await self.post(
                f"{self.uri.path}?rpc=json",
                json=jsonrpcclient.request(
                    method,
                    params=params,
                ),
                headers={"Content-Type": "application/json"},
            )
            return jsonrpcclient.parse(v.json())
        except Exception as e:
            raise RtorrentClientError(e) from None


class RtorrentTransport(AsyncBaseTransport):
    """Transport for SCGI+UNIX and SSH+UNIX."""

    def __init__(self, uri: URL):
        """Initialize the transport."""
        self.uri = uri
        super().__init__()

    async def handle_async_request(
        self,
        request: Request,
    ) -> Response:
        """Handle the request."""

        async def _json(
            target: str,
            request: Request,
        ) -> tuple[dict[str, typing.Any], dict[str, str]]:
            headers = []
            headers.append(("CONTENT_TYPE", "application/json"))
            if self.uri.scheme == "scgi+unix":
                async with await connect_unix(target) as _socket:
                    _input = _encode_payload(request.content, headers)
                    await _socket.send(_input)
                    c = await _socket.receive()
                    payload, parsed_headers = _parse_response(c)
                    return msgspec.json.decode(payload), parsed_headers
            elif self.uri.scheme == "ssh+unix":
                cmd = [
                    "ssh",
                    "-T",
                    self.uri.host,
                    "-p",
                    str(self.uri.port),
                    "--",
                    "socat",
                    "-t5",
                    "-dd",
                    "STDIO",
                    target,
                ]
                try:
                    from anyio import run_process

                    cmd_input = _encode_payload(request.content, headers)
                    logger.debug("cmd_input: %s", cmd_input)
                    cmd_out = await run_process(cmd, input=cmd_input, check=False)
                    payload, parsed_headers = _parse_response(cmd_out.stdout)
                    return msgspec.json.decode(payload), parsed_headers
                except Exception as e:
                    logger.error(e)
                    raise e
            elif self.uri.scheme == "scgi":
                if self.uri.port is None:
                    raise ValueError("Port is required for SCGI")
                async with await connect_tcp(self.uri.host, self.uri.port) as _socket:
                    _input = _encode_payload(request.content, headers)
                    await _socket.send(_input)
                    received_data = b""
                    while True:
                        try:
                            chunk = await _socket.receive(1024)
                            if not chunk:
                                break
                            received_data += chunk
                        except EndOfStream:
                            break
                        except Exception as critical_error:
                            logger.critical(f"CRITICAL: {critical_error!s}")
                            raise critical_error
                    logger.debug(f"Received: {len(received_data)}")
                    payload, parsed_headers = _parse_response(received_data)
                    return msgspec.json.decode(payload), parsed_headers
            else:
                raise ValueError("Invalid scheme")

        try:
            _payload, _headers = await _json(request.url.path, request)
        except EndOfStream:
            raise SocketError("End of stream") from None
        except Exception as e:
            raise e
        return Response(
            200,
            headers=_headers,
            json=_payload,
        )

    @staticmethod
    def parse_response(response: io.BytesIO) -> dict[str, typing.Any]:
        """Parse the response from the server."""
        # if self.codec == xmlrpclib:
        #     return super().parse_response(response)
        try:
            parsed_ = json.loads(response.read())
        except Exception as e:
            raise e
        return parsed_


def _encode_netstring(data: bytes) -> bytes:
    """Encode data as netstring."""
    return b"%d:%s," % (len(data), data)


def _encode_headers(headers: list[tuple[str, str]]) -> bytes:
    """Make SCGI header bytes from list of tuples."""
    return b"".join(
        [b"%s\0%s\0" % (k.encode("ascii"), v.encode("ascii")) for k, v in headers]
    )


def _encode_payload(data: bytes, headers: list[tuple[str, str]] | None = None) -> bytes:
    """Wrap data in an SCGI request."""
    prolog: bytes = b"CONTENT_LENGTH\0%d\0SCGI\x001\0" % len(data)
    if headers:
        prolog += _encode_headers(headers)

    return _encode_netstring(prolog) + data


def _parse_headers(headers: bytes) -> dict[str, str]:
    """Get headers dict from header bytestring.

    :param headers: An SCGI header bytestring
    :type headers: bytes
    :return: A dictionary of string keys/values
    """
    try:
        result = {}
        for line in headers.splitlines():
            if line:
                key, value = line.rstrip().split(b": ", 1)
                result[key.decode("ascii")] = value.decode("ascii")
        return result
    except (TypeError, ValueError) as exc:
        raise SCGIError(f"Error in SCGI headers {headers.decode()}") from exc


class SCGIError(Exception):
    """Base exception for SCGI transport errors."""


def _parse_response(resp: bytes) -> tuple[bytes, dict[str, str]]:
    """Get xmlrpc response from scgi response."""
    # Assume they care for standards and send us CRLF (not just LF)
    try:
        headers, payload = resp.split(b"\r\n\r\n", 1)
    except (TypeError, ValueError) as exc:
        raise SCGIError(
            f"No header delimiter in SCGI response of length {len(resp)}"
        ) from exc
    parsed_headers = _parse_headers(headers)

    # clen = parsed_headers.get("Content-Length")
    # if clen is not None:
    #     # Check length, just in case the transport is bogus
    #     if len(payload) != int(clen):
    #         logger.critical("Content-Length mismatch: %d != %d", len(payload), clen)
    #         # raise SCGIError(f"Content-Length mismatch: {len(payload)} != {clen}")
    #
    return payload, parsed_headers


class SocketError(Exception):
    """Base exception for socket errors."""
