"""Abstract base class for torrent clients."""

from abc import ABC, abstractmethod
from typing import Any, Iterable

from httpx import AsyncClient
from jsonrpcclient import Error, Ok

from yatg.models import (
    STATUS_ICONS,
    AddTorrentFormFile,
    AddTorrentFormURL,
    TorrentInfo,
    TorrentInfoDeluge,
    TorrentPeersInfo,
    TorrentStatus,
    TrackerInfo,
)

JSONEncoderResponse = Ok | Error | Iterable[Ok | Error]


class TorrentAsyncClient(ABC, AsyncClient):
    """Client for interacting with various torrent daemons."""

    @abstractmethod
    async def session_info(self) -> JSONEncoderResponse:
        """Get session info."""
        raise NotImplementedError

    @abstractmethod
    async def get_tags(self) -> JSONEncoderResponse:
        """Get tags."""
        raise NotImplementedError

    @abstractmethod
    async def get_trackers(self, _hash: str) -> JSONEncoderResponse:
        """Get trackers."""
        raise NotImplementedError

    @abstractmethod
    async def load_torrent(
        self,
        data: AddTorrentFormFile | AddTorrentFormURL,
    ) -> list[JSONEncoderResponse]:
        """Load torrent."""
        raise NotImplementedError

    @abstractmethod
    async def start_torrent(
        self, _hash: str
    ) -> tuple[
        JSONEncoderResponse,
        JSONEncoderResponse,
    ]:
        """Start torrent."""
        raise NotImplementedError

    @abstractmethod
    async def stop_torrent(
        self, _hash: str
    ) -> tuple[
        JSONEncoderResponse,
        JSONEncoderResponse,
    ]:
        """Stop torrent."""
        raise NotImplementedError

    @abstractmethod
    async def delete_torrent(self, _hash: str) -> JSONEncoderResponse:
        """Delete torrent."""
        raise NotImplementedError

    @abstractmethod
    async def check_hash(self, _hash: str) -> JSONEncoderResponse:
        """Pause torrent."""
        raise NotImplementedError

    @abstractmethod
    async def get_torrents_list(
        self, _hash: str | None = None
    ) -> list[TorrentInfo] | list[TorrentInfoDeluge]:
        """Get torrents list."""
        raise NotImplementedError

    @abstractmethod
    async def get_tracker(self, _hash: str) -> list[TrackerInfo] | None:
        """Get tracker."""
        raise NotImplementedError

    @abstractmethod
    async def get_peers(self, _hash: str | None) -> list[TorrentPeersInfo]:
        """Get peers."""
        raise NotImplementedError

    @abstractmethod
    async def set_tags(self, _hash: str, tags: str) -> JSONEncoderResponse:
        """Get tags."""
        raise NotImplementedError

    def _get_stats(
        self,
        torrents: list[TorrentInfo] | list[TorrentInfoDeluge],
    ) -> tuple[dict[str | Any, tuple[int, str]], dict[str, int], dict[str, int]]:
        """Get statistics from a list of torrents."""
        if any([hasattr(x, "custom1") for x in torrents]):  # noqa: C419
            all_tags = [
                tag
                for tag_sublist in [t.custom1.split(",") for t in torrents]  # type: ignore[union-attr]
                for tag in tag_sublist
                if tag != ""
            ]
        else:
            all_tags = []
        tags_count = {item: all_tags.count(item) for item in set(all_tags)}
        all_statuses = [
            status
            for status_sublist in [t.statuses for t in torrents]
            for status in status_sublist  # type: ignore[union-attr]
        ]
        all_statuses_plus_missing = set(all_statuses) | set(TorrentStatus)
        statuses_count = {
            item: (all_statuses.count(item), STATUS_ICONS[item])  # type: ignore[index]
            for item in list(all_statuses_plus_missing)
        }
        all_trackers = [
            tracker.url
            # urlparse(tracker.url).netloc
            for trackers_sublist in [t.trackers for t in torrents]
            for tracker in trackers_sublist  # type: ignore[union-attr]
            # if urlparse(tracker.url).scheme != "dht"
        ]
        trackers_count = {item: all_trackers.count(item) for item in set(all_trackers)}
        return statuses_count, tags_count, trackers_count

    @abstractmethod
    async def get_info_files(self, _hash: str) -> JSONEncoderResponse:
        """Get info."""
        raise NotImplementedError
