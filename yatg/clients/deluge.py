"""Deluge client for the Deluge RPC."""

import base64
import datetime
import logging
import ssl
import struct
import typing
import zlib
from types import TracebackType

import jsonrpcclient
import msgspec
from anyio import BrokenResourceError, EndOfStream, connect_tcp
from anyio.streams.tls import TLSStream
from httpx import URL, AsyncBaseTransport, Request, Response
from jsonrpcclient import Ok
from msgspec.json import decode
from rencode import dumps, loads

from yatg.clients.base import JSONEncoderResponse, TorrentAsyncClient
from yatg.clients.rtorrent import SocketError
from yatg.models import (
    AddTorrentFormFile,
    AddTorrentFormURL,
    TorrentInfoDeluge,
    TorrentPeersInfo,
    TorrentStatus,
    TrackerInfo,
)

logger = logging.getLogger(__name__)

MESSAGE_HEADER_SIZE = 5
READ_SIZE = 10


class DelugeClientError(Exception):
    """Base exception for all deluge client exceptions."""


class ConnectionLostError(DelugeClientError):
    """Exception for connection lost errors."""


class RemoteError(DelugeClientError):
    """Exception for remote errors."""


def get_torrent_status_from_statuses(
    progress: float, state: str, download_payload_rate: int, upload_payload_rate: int
) -> list[str]:
    """Returns a list of torrent statuses based on input parameters.

    Args:
        progress (float): Torrent progress percentage.
        state (str): Torrent status (e.g., 'Checking', 'Downloading', etc.).
        download_payload_rate (int): Download payload rate in bytes per second.
        upload_payload_rate (int): Upload payload rate in bytes per second.

    Returns:
        List[str]: A list of torrent statuses.
    """
    torrent_status: list[str] = []

    if state == "Checking":
        torrent_status.append(TorrentStatus.CHECKING)
    elif state in ["Downloading", "Queued"]:
        torrent_status.append(TorrentStatus.DOWNLOADING)
    elif state == "Seeding":
        torrent_status.append(TorrentStatus.SEEDING)
    elif state == "Paused":
        torrent_status.append(TorrentStatus.STOPPED)
    else:
        torrent_status.append(TorrentStatus.ERROR)

    if progress == 100:
        torrent_status.append(TorrentStatus.COMPLETE)

    if download_payload_rate > 0 or upload_payload_rate > 0:
        torrent_status.append(TorrentStatus.ACTIVE)
    else:
        torrent_status.append(TorrentStatus.INACTIVE)

    return torrent_status


class DelugeClient(TorrentAsyncClient):
    """A json-rcp capable deluge client using a custom httpx transport."""

    def __init__(self, uri: URL):
        """Initialize the deluge client."""
        self.uri = uri
        super().__init__(
            transport=DelugeTransport(uri), base_url=f"{self.uri.scheme}://"
        )

    async def __aexit__(
        self,
        exc_type: type[BaseException] | None = None,
        exc_value: BaseException | None = None,
        traceback: TracebackType | None = None,
    ) -> None:
        """Close the connection."""
        if hasattr(self._transport, "_socket"):
            if self._transport._socket:
                await self._transport._socket.aclose()
        await super().__aexit__(exc_type, exc_value, traceback)

    async def connect(self) -> JSONEncoderResponse:
        """Connect to daemon."""
        return await self.send_daemon(
            "daemon.login",
            self.uri.username,
            self.uri.password,
            client_version="deluge-client",
        )

    async def session_info(self) -> JSONEncoderResponse:
        """Get the session info."""
        return await self.send_daemon(method="daemon.info")

    async def get_tags(self) -> JSONEncoderResponse:
        """Get the tags."""
        return Ok(result=[], id=0)

    async def get_trackers(self, _hash: str) -> JSONEncoderResponse:
        """Get the trackers."""
        return Ok(result=[], id=123)

    async def get_peers(self, _hash: str | None = None) -> list[TorrentPeersInfo]:
        """Get peers."""
        return [TorrentPeersInfo()]  # type: ignore[call-arg]

    async def set_tags(self, _hash: str | None, tags: str) -> JSONEncoderResponse:
        """Set tags."""
        return Ok(result=[tags], id=789)

    async def load_torrent(
        self, data: AddTorrentFormFile | AddTorrentFormURL
    ) -> list[JSONEncoderResponse]:
        """Load torrents."""
        results = []
        if data.raw_data:
            for raw_data in data.raw_data:
                d = base64.b64encode(raw_data).decode()
                if isinstance(data, AddTorrentFormURL):
                    added = await self.send_daemon(
                        "core.add_torrent_file_async", data.urls[0], d, {}
                    )
                else:
                    added = await self.send_daemon(
                        "core.add_torrent_file_async",
                        data.paths[0].filename,
                        d,
                        {},
                    )
                results.append(added)
        return results

    async def start_torrent(
        self, _hash: str
    ) -> tuple[JSONEncoderResponse, JSONEncoderResponse]:
        """Start a torrent."""
        start = await self.send_daemon("core.start_torrent", _hash)
        return start, Ok(result=[f"Deluge started: {start}"], id=333)

    async def stop_torrent(
        self, _hash: str
    ) -> tuple[JSONEncoderResponse, JSONEncoderResponse]:
        """Stop a torrent."""
        stop = await self.send_daemon("core.pause_torrent", _hash)
        return stop, Ok(result=[f"Deluge stopped: {stop}"], id=444)

    async def delete_torrent(self, _hash: str) -> JSONEncoderResponse:
        """Delete a torrent."""
        return await self.send_daemon("core.remove_torrent", _hash, remove_data=False)

    async def check_hash(self, _hash: str) -> JSONEncoderResponse:
        """Checks a given ash."""
        return await self.send_daemon("core.force_recheck", _hash)

    async def get_info_files(self, _hash: str) -> JSONEncoderResponse:
        """Return files info for a iven hash."""
        return await self.send_daemon("core.info")

    async def get_torrents_list(
        self, _hash: str | None = None
    ) -> list[TorrentInfoDeluge]:
        """Returns a list of torrents."""
        h = await self.send_daemon(
            "core.get_torrents_status",
            {},
            (
                "active_time",
                "comment",
                "download_location",
                "download_payload_rate",
                "eta",
                "finished_time",
                "message",
                "name",
                "num_peers",
                "num_seeds",
                "private",
                "progress",
                "ratio",
                "sequential_download",
                "state",
                "super_seeding",
                "time_added",
                "total_done",
                "total_payload_download",
                "total_payload_upload",
                "total_peers",
                "total_size",
                "total_seeds",
                "tracker_host",
                "upload_payload_rate",
            ),
            {},
        )
        torrents_info = []

        if isinstance(h, Ok):
            for hash_, status in h.result.items():
                status_new = status
                status_new["date_active"] = (
                    -1
                    if (
                        status["download_payload_rate"]
                        > 0 | status["upload_payload_rate"]
                        > 0
                    )
                    else status["active_time"]
                )
                status_new["date_finished"] = (
                    datetime.datetime.now(datetime.UTC)
                    - datetime.timedelta(seconds=status["finished_time"])
                    if status["finished_time"] > 0
                    else 0
                )
                status_new["eta"] = -1 if not status["eta"] else status["eta"]
                status_new["statuses"] = get_torrent_status_from_statuses(
                    progress=status["progress"],
                    state=status["state"],
                    download_payload_rate=status["download_payload_rate"],
                    upload_payload_rate=status["upload_payload_rate"],
                )
                status_new["date_added"] = status["time_added"]
                status_new["creation_date"] = 0
                status_new["directory"] = status["download_location"]
                status_new["down_rate"] = status["download_payload_rate"]
                status_new["down_total"] = status["total_payload_download"]
                status_new["up_rate"] = status["upload_payload_rate"]
                status_new["up_total"] = status["total_payload_upload"]
                status_new["hash"] = hash_
                status_new["is_private"] = status["private"]
                status_new["is_initial_seeding"] = status["super_seeding"]
                status_new["is_sequential"] = status["sequential_download"]
                status_new["percent_complete"] = status["progress"]
                status_new["peers_connected"] = status["num_peers"]
                status_new["peers_total"] = (
                    0 if status["total_peers"] < 0 else status["total_peers"]
                )
                status_new["seeds_connected"] = status["num_seeds"]
                status_new["seeds_total"] = (
                    0 if status["total_seeds"] < 0 else status["total_seeds"]
                )
                status_new["size_bytes"] = status["total_size"]
                status_new["tags"] = []
                status_new["trackers"] = [TrackerInfo(url=status["tracker_host"])]
                status_new["priority"] = 1
                status_new["bytes_done"] = status["total_done"]

                torrent_deluge_info = msgspec.convert(
                    status_new,
                    type=TorrentInfoDeluge,
                    strict=False,
                    builtin_types=(datetime.datetime),  # type: ignore[call-overload]
                )
                torrents_info.append(torrent_deluge_info)
        return torrents_info

    async def get_tracker(self, _hash: str | None = None) -> list[TrackerInfo] | None:
        """Get trackers for a given hash."""
        torrents = await self.get_torrents_list(_hash=_hash)
        return next(t.trackers for t in torrents)

    async def send_daemon(
        self, method: str, *args: typing.Any, **kwargs: typing.Any
    ) -> JSONEncoderResponse:
        """Generic method to send a rpc call to the deluge client."""
        try:
            if not self._transport._socket:  # type: ignore[attr-defined]
                ssl_context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS_CLIENT)
                ssl_context.check_hostname = False
                ssl_context.verify_mode = ssl.CERT_NONE
                assert self.uri.port  # noqa: S101
                self._transport._socket = await connect_tcp(  # type: ignore[attr-defined]
                    self.uri.host, self.uri.port, ssl_context=ssl_context
                )
            v = await self.post(
                "/",
                json=jsonrpcclient.request(
                    method, params={"args": args, "kwargs": kwargs}
                ),
                headers=(("Content-Type", "application/json"),),
            )

            return jsonrpcclient.parse(v.json())
        except BrokenResourceError as e:
            self._transport._socket = None  # type: ignore[attr-defined]
            raise ConnectionLostError(e) from e
        except Exception as e:
            raise DelugeClientError(e) from e


class DelugeTransport(AsyncBaseTransport):
    """Transport for the Deluge RPC."""

    def __init__(self, uri: URL):
        """Initialize the transport."""
        self.uri = uri
        self.request_id = 0
        self._socket = None

    async def _send_call(
        self, _socket: TLSStream, req: tuple[tuple[str, str, typing.Any, typing.Any]]
    ) -> None:
        compressed_req = zlib.compress(dumps(req))
        await _socket.send(struct.pack("!BI", 1, len(compressed_req)))
        await _socket.send(compressed_req)

    async def _receive_response(
        self, _socket: TLSStream, partial_data: bytes = b""
    ) -> bytes:
        expected_bytes = None
        data = partial_data
        while True:
            try:
                d = await _socket.receive(READ_SIZE)
            except Exception as e:
                raise e

            if len(d) == 0:
                raise ConnectionLostError()

            data += d
            if expected_bytes is None:
                if len(data) < 5:
                    continue

                header = data[:MESSAGE_HEADER_SIZE]
                data = data[MESSAGE_HEADER_SIZE:]
                expected_bytes = struct.unpack("!I", header[1:])[0]

            if len(data) >= expected_bytes:
                data = zlib.decompress(data)
                break

        return data

    @staticmethod
    def _decode_response(data: bytes) -> typing.Any:
        data_decoded = list(loads(data, decode_utf8=True))
        msg_type = data_decoded[0]
        _request_id = data_decoded[1]
        match msg_type:
            case 1:
                return data_decoded[2]
            case 2:
                exception_type, exception_msg, _, traceback = data_decoded[2:]
                exception_msg = ", ".join(str(x) for x in exception_msg)
                exception = type(str(exception_type), (RemoteError,), {})
                exception_msg = f"{exception_msg}\n{traceback}"
                raise exception(exception_msg)

    async def handle_async_request(
        self,
        request: Request,
    ) -> Response:
        """Handle the request."""

        async def _json(
            target: str,  # noqa: ARG001
            request: Request,
        ) -> dict[str, typing.Any]:
            if self.uri.scheme == "http":
                if self.uri.port is None:
                    raise ValueError("Port is required for deluge RPC")
                _id = decode(request.content).get("id")
                method = decode(request.content).get("method")
                args = decode(request.content).get("params").get("args")
                kwargs = decode(request.content).get("params").get("kwargs")
                req = ((_id, method, args, kwargs),)
                assert self._socket  # noqa: S101
                await self._send_call(self._socket, req)
                assert self._socket  # noqa: S101
                response = await self._receive_response(
                    self._socket,
                )
                return {
                    "id": _id,
                    "jsonrpc": "2.0",
                    "result": self._decode_response(response),
                }
            raise ValueError("Invalid scheme")

        try:
            json = await _json(request.url.path, request)
        except EndOfStream:
            raise SocketError("End of stream") from None
        except Exception as e:
            raise e
        except BaseException as e:
            raise e
        return Response(
            200,
            json=json,
        )
