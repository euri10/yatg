"""App building and configuration."""

from contextlib import asynccontextmanager
from functools import partial
from pathlib import Path
from typing import Any, AsyncGenerator

import asyncpg
import msgspec
from asyncpg import Pool
from httpx import AsyncClient
from jinja2 import Environment, FileSystemLoader, select_autoescape
from litestar import Litestar
from litestar.connection import ASGIConnection
from litestar.contrib.htmx.request import HTMXRequest
from litestar.contrib.jinja import JinjaTemplateEngine
from litestar.di import Provide
from litestar.middleware.session.server_side import (
    ServerSideSessionBackend,
    ServerSideSessionConfig,
)
from litestar.plugins.flash import FlashConfig, FlashPlugin
from litestar.security.session_auth import SessionAuth
from litestar.static_files import create_static_files_router
from litestar.stores.file import FileStore
from litestar.template import TemplateConfig

from yatg.dependencies import get_asyncpg_connection, get_torrent_client
from yatg.models import User
from yatg.settings import AppSettings
from yatg.utils import duration_format


@asynccontextmanager
async def lifespan(
    _app: Litestar,
    app_settings: AppSettings,
) -> AsyncGenerator[None, None]:
    """Setup and teardown for the app."""
    tclient = AsyncClient()
    _app.state.tclient = tclient
    asyncpg_pool = await asyncpg.create_pool(app_settings.postgres_url)
    _app.state.asyncpg_pool = asyncpg_pool
    _app.state.clients = {}
    try:
        yield
    finally:
        await tclient.aclose()
        asyncpg_pool.terminate()
        await asyncpg_pool.close()
        for _, client_value in _app.state.clients.items():
            if hasattr(client_value._transport, "_socket"):
                if client_value._transport._socket:
                    await client_value._transport._socket.aclose()
            await client_value.aclose()


async def retrieve_user_handler(
    session: dict[str, Any], connection: ASGIConnection[Any, Any, Any, Any]
) -> User | None:
    """Retrieve the user from the session."""
    if session.get("user_id"):
        asyncpg_pool: Pool = connection.app.state.get("asyncpg_pool")
        async with asyncpg_pool.acquire() as asyncpg_connection:
            user_db = await asyncpg_connection.fetchrow(
                "select id as user_id, username from users where id = $1",
                session["user_id"],
            )
            return msgspec.convert(user_db, User, from_attributes=True)
    return None


def create_app(app_settings: AppSettings) -> Litestar:
    """Create the yatg app."""
    from yatg.routers.auth import auth_router
    from yatg.routers.home import home_router
    from yatg.routers.rtorrent import rtorrent_router
    from yatg.routers.settings import settings_router

    templates_dirs = Path(__file__).parent / "templates"
    environment = Environment(
        loader=FileSystemLoader(templates_dirs),
        lstrip_blocks=True,
        trim_blocks=True,
        autoescape=select_autoescape(
            enabled_extensions=("html", "jinja", "jinja2"),
            default_for_string=True,
        ),
    )
    environment.filters["datetime_format"] = (
        lambda value, format_str="%Y-%m-%d %H:%M:%S": value.strftime(format_str)
    )
    environment.filters["duration_format"] = duration_format

    template_config = TemplateConfig(
        instance=JinjaTemplateEngine.from_environment(environment)
    )
    session_config = ServerSideSessionConfig()
    session_auth = SessionAuth[User, ServerSideSessionBackend](
        retrieve_user_handler=retrieve_user_handler,
        session_backend_config=session_config,
        exclude=["^/$", "/auth/.*", "/settings/*", "/static/.*"],
    )
    from yatg.dependencies import get_tclient

    return Litestar(
        route_handlers=[
            create_static_files_router(
                path="/static", directories=[Path(__file__).parent / "static"]
            ),
            home_router,
            auth_router,
            rtorrent_router,
            settings_router,
        ],
        template_config=template_config,
        dependencies={
            "app_settings": Provide(lambda: app_settings, sync_to_thread=False),
            "asyncpg_connection": Provide(get_asyncpg_connection),
            "tclient": Provide(get_tclient),
            "torrent_client": Provide(get_torrent_client),
        },
        debug=app_settings.debug,
        lifespan=[partial(lifespan, app_settings=app_settings)],
        request_class=HTMXRequest,
        plugins=[
            FlashPlugin(
                config=FlashConfig(
                    template_config=template_config,
                )
            )
        ],
        middleware=[session_config.middleware],
        stores={"sessions": FileStore(path=app_settings.session_path)},
        on_app_init=[session_auth.on_app_init],
    )
