"""Settings module."""

import os
from pathlib import Path

import msgspec
from dotenv import load_dotenv

from yatg.utils import dec_hook


class AppSettings(msgspec.Struct):
    """Settings for the application."""

    postgres_url: str
    session_path: Path
    debug: bool = False

    @classmethod
    def from_env(
        cls, env_file: Path = Path(".env"), prefix: str = "YATG_"
    ) -> "AppSettings":
        """Load settings from environment variables."""
        load_dotenv(env_file, verbose=True)
        transformed_keys = {
            k.replace(prefix, "").lower(): v
            for k, v in os.environ.items()
            if k.startswith(prefix)
        }
        return msgspec.convert(
            transformed_keys, type=AppSettings, strict=False, dec_hook=dec_hook
        )
