"""Dependencies for the application."""

from typing import AsyncGenerator

from asyncpg import Connection
from httpx import AsyncClient
from litestar.datastructures import State

from yatg.clients.base import TorrentAsyncClient


async def get_asyncpg_connection(state: State) -> AsyncGenerator[Connection, None]:
    """Get a connection to the asyncpg pool."""
    async with state.asyncpg_pool.acquire() as connection:
        yield connection


async def get_tclient(state: State) -> AsyncGenerator[AsyncClient, None]:
    """Get a client for fetching torrents from a URL."""
    tclient = state.tclient
    yield tclient


async def get_torrent_client(
    state: State, setting_id: int
) -> AsyncGenerator[TorrentAsyncClient, None]:
    """Get a client for interacting with various torrent daemons."""
    _client = state.clients.get(setting_id)
    yield _client
