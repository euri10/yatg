"""Entrypoint for the application."""

import logging.config
from pathlib import Path

import yaml

from yatg.app import create_app
from yatg.settings import AppSettings

with Path("yatg/logging_dev.yaml").open() as log_file:
    config = yaml.safe_load(log_file.read())
    logging.config.dictConfig(config)

app_settings = AppSettings.from_env()

app = create_app(app_settings=app_settings)
