-- Drop index on settings table
DROP INDEX IF EXISTS idx_settings_user_id;

-- Drop settings table
DROP TABLE IF EXISTS settings;

-- Drop users table
DROP TABLE IF EXISTS users;
