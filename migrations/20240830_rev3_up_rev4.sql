alter table settings
    add alias text default gen_random_uuid() not null;
drop index idx_settings_client_uri_type;
create unique index settings_uindex
    on settings (alias, client, uri, uri_type, user_id);
