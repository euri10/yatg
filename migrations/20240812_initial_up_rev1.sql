-- Create users table
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(50) NOT NULL UNIQUE,
  password_hash VARCHAR(255) NOT NULL
);

-- Create settings table with a foreign key reference to users
CREATE TABLE settings (
  id SERIAL PRIMARY KEY,
  client VARCHAR(20) CHECK(client IN ('rtorrent', 'deluge', 'transmission')) NOT NULL,
  uri VARCHAR(1000),
  uri_type VARCHAR(50),
  user_id INTEGER NOT NULL REFERENCES users(id)
);

-- Create index on settings table for faster joins
CREATE INDEX idx_settings_user_id ON settings (user_id);
