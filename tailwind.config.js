/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin')

module.exports = {
    content: ["./yatg/**/*.{html,js}"],
    theme: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
        plugin(function ({addVariant}) {
            addVariant('progress-unfilled', ['&::-webkit-progress-bar', '&']);
            addVariant('progress-filled', ['&::-webkit-progress-value', '&::-moz-progress-bar', '&']);
        })
    ],
}
