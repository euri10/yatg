import pytest
from litestar.testing import AsyncTestClient
from lxml.html import fromstring


@pytest.mark.anyio
async def test_home(client: AsyncTestClient) -> None:
    response = await client.get("/")
    assert response.status_code == 200
    html = fromstring(response.text)
    assert html.xpath("//title")[0].text == "Home - yatg"
    divs = html.xpath("//div[@hx-get]")
    assert len(divs) == 2
    assert divs[0].get("hx-get") == "/auth/login"
    assert divs[0].text.strip() == "Login"
    assert divs[1].get("hx-get") == "/auth/signup"
    assert divs[1].text.strip() == "Sign up"


@pytest.mark.anyio
async def test_tabs(
    client: AsyncTestClient, rtorrent_port_existing_user: dict[str, str]
) -> None:
    response = await client.get("/torrents")
    assert response.status_code == 200
    html = fromstring(response.text)
    assert html.xpath("//title")[0].text == "torrents - yatg"
    # check the selected tab is tab-rtorrent-1 and the last tab is
    # tab-settings and is not selected
    assert (
        html.xpath("//div[@id='my_rtorrent_port']/*/span")[0].text == "my_rtorrent_port"
    )
    assert html.xpath("//div[@id='my_rtorrent_port']")[0].get("aria-selected") == "true"
    assert "+" in html.xpath("//div[@id='tab-settings']/span")[0].text
    assert html.xpath("//div[@id='tab-settings']")[0].get("aria-selected") == "false"
