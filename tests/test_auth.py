import os
from urllib.parse import quote

import pytest
from litestar.testing import AsyncTestClient
from lxml.html import fromstring

from tests.conftest import (
    RTORRENT_SOCKET_SOCKET_PATH,
)


@pytest.mark.anyio
async def test_rtorrent_check(client: AsyncTestClient) -> None:
    if os.environ.get("CI") == "true":  # pragma: no cover
        uri = "scgi://rtorrent:50005"
    else:
        uri = "scgi://localhost:50005"
    response = await client.get(f"/auth/rtorrent_check?uri={uri}")
    assert response.status_code == 200
    html = fromstring(response.text)
    assert len(html.xpath("//button[@id='save-settings-button']")) == 1
    assert (
        "Save rtorrent settings"
        in html.xpath("//button[@id='save-settings-button']")[0].text
    )


@pytest.mark.anyio
async def test_rtorrent_connection_failed(client: AsyncTestClient) -> None:
    uri = "scgi://localhost:11111"
    response = await client.get(f"/auth/rtorrent_check?uri={uri}")
    assert response.status_code == 200
    assert "Connection failed" in response.text


@pytest.mark.anyio
async def test_rtorrent_wrong_scheme(client: AsyncTestClient) -> None:
    uri = "http://localhost:50005"
    response = await client.get(f"/auth/rtorrent_check?uri={uri}")
    assert response.status_code == 200
    assert "Connection failed: Invalid scheme" in response.text


@pytest.mark.anyio
async def test_rtorrent_socket(client: AsyncTestClient) -> None:
    uri = "scgi+unix:///tmp/rtorrent.sock"
    response = await client.get(f"/auth/rtorrent_check?uri={uri}")
    assert "Connection failed" in response.text
    uri = quote(f"scgi+unix://{RTORRENT_SOCKET_SOCKET_PATH}")
    response = await client.get(f"/auth/rtorrent_check?uri={uri}")
    assert response.status_code == 200
    html = fromstring(response.text)
    assert len(html.xpath("//button[@id='save-settings-button']")) == 1
    assert (
        "Save rtorrent settings"
        in html.xpath("//button[@id='save-settings-button']")[0].text
    )


# @pytest.mark.anyio
# async def test_deluge_wrong_scheme(client: AsyncTestClient) -> None:
#     uri = "scgi://localhost:50005"
#     response = await client.get(f"/auth/deluge_check?uri={uri}")
#     assert response.status_code == 200
#     assert "Connection failed: Invalid scheme" in response.text
#
#
# @pytest.mark.anyio
# async def test_deluge_check_daemon_error(client: AsyncTestClient) -> None:
#     uri = "http://localhost:58847"
#     mock_deluge_client = mock.AsyncMock()
#     mock_deluge_client.send_daemon.return_value = Error(
#         code=-32601, message="Method not found", data=None, id=1
#     )
#
#     with mock.patch("yatg.routers.auth.DelugeClient", return_value=mock_deluge_client):  # noqa: E501
#         response = await client.get(f"/auth/deluge_check?uri={uri}")
#         assert response.status_code == 200
#         assert (
#             "<h1>Connection failed: Error(code=-32601, message='Method not found', data=None, id=1)</h1>"  # noqa: E501
#             in response.text
#         )
#


@pytest.mark.anyio
async def test_signup(
    client: AsyncTestClient,
) -> None:
    response = await client.get(
        "/auth/signup",
    )
    assert response.status_code == 200
    # check that the form is present with username and password fields
    html = fromstring(response.text)
    assert html.xpath("//input[@name='username']")
    assert html.xpath("//input[@name='password']")
    assert response.url == "http://testserver.local/auth/signup"


@pytest.mark.anyio
async def test_login_rtorrent_port(
    client: AsyncTestClient,
    rtorrent_port_existing_user: dict[str, str],
) -> None:
    response = await client.get(
        "/auth/login",
    )
    assert response.status_code == 200
    # check that the form is present with username and password fields
    html = fromstring(response.text)
    assert html.xpath("//input[@name='username']")
    assert html.xpath("//input[@name='password']")
    response = await client.post(
        "/auth/login",
        data={
            "username": rtorrent_port_existing_user["username"],
            "password": rtorrent_port_existing_user["password"],
        },
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert response.url == "http://testserver.local/torrents"


@pytest.mark.anyio
async def test_login_rtorrent_socket(
    client: AsyncTestClient,
    rtorrent_socket_existing_user: dict[str, str],
) -> None:
    response = await client.get(
        "/auth/login",
    )
    assert response.status_code == 200
    # check that the form is present with username and password fields
    html = fromstring(response.text)
    assert html.xpath("//input[@name='username']")
    assert html.xpath("//input[@name='password']")
    response = await client.post(
        "/auth/login",
        data={
            "username": rtorrent_socket_existing_user["username"],
            "password": rtorrent_socket_existing_user["password"],
        },
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert response.url == "http://testserver.local/torrents"


@pytest.mark.anyio
async def test_login_redirects_if_no_settings(
    client: AsyncTestClient, rtorrent_port_existing_user: dict[str, str]
) -> None:
    # delete the settings
    response = await client.delete("/settings/my_rtorrent_port")
    assert response.status_code == 200
    response = await client.get(
        "/auth/logout",
    )
    assert response.status_code == 200
    response = await client.get("/auth/login")
    assert response.status_code == 200
    response = await client.post(
        "/auth/login",
        data={
            "username": rtorrent_port_existing_user["username"],
            "password": rtorrent_port_existing_user["password"],
        },
    )
    assert response.status_code == 200
    assert response.url == "http://testserver.local/settings"


@pytest.mark.anyio
async def test_authenticate_user_not_found(client: AsyncTestClient) -> None:
    response = await client.post(
        "/auth/login",
        data={"username": "notfound", "password": "notfound"},
        follow_redirects=True,
    )
    assert response.status_code == 401


@pytest.mark.anyio
async def test_authenticate_user_wrong_password(
    client: AsyncTestClient, rtorrent_port_existing_user: dict[str, str]
) -> None:
    response = await client.post(
        "/auth/login",
        data={"username": rtorrent_port_existing_user["username"], "password": "wrong"},
        follow_redirects=True,
    )
    assert response.status_code == 401


@pytest.mark.anyio
async def test_deluge_check(client: AsyncTestClient) -> None:
    if os.environ.get("CI") == "true":  # pragma: no cover
        uri = "http://localclient:deluge@deluge:58846"
    else:
        uri = "http://localclient:deluge@localhost:58847"
    response = await client.get(f"/auth/deluge_check?uri={uri}")
    assert response.status_code == 200
    html = fromstring(response.text)
    assert len(html.xpath("//button[@id='save-settings-button']")) == 1
    assert (
        "Save deluge settings"
        in html.xpath("//button[@id='save-settings-button']")[0].text
    )


#
# @pytest.mark.anyio
# async def test_deluge_connection_failed(client: AsyncTestClient) -> None:
#     uri = "http://localhost:11111"
#     response = await client.get(f"/auth/deluge_check?uri={uri}")
#     assert response.status_code == 200
#     assert "Connection failed" in response.text
