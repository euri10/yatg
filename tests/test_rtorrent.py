import io
from pathlib import Path

import aiofiles
import pytest
from litestar.testing import AsyncTestClient
from lxml.html import fromstring


@pytest.mark.anyio
async def test_add_torrent(
    client: AsyncTestClient,
    rtorrent_port_existing_user: dict[str, str],
) -> None:
    response = await client.get("/add/menu/1")
    assert response.status_code == 200
    assert response.url == "http://testserver.local/add/menu/1"


@pytest.mark.anyio
async def test_add_by_url(
    client: AsyncTestClient,
    rtorrent_port_existing_user: dict[str, str],
) -> None:
    response = await client.get("/add/by_url/1")
    assert response.status_code == 200
    assert response.url == "http://testserver.local/add/by_url/1"
    # check there is only one url entry possible
    html = fromstring(response.text)
    url_inputs = html.xpath("//div[@id='url_items']/div/input")
    assert len(url_inputs) == 1
    # add another url
    response = await client.get("/add/by_url/1/item/1/0")
    assert response.status_code == 200
    # then another url
    response = await client.get("/add/by_url/1/item/2/1")
    assert response.status_code == 200
    # check there are now three url entries possible
    html = fromstring(response.text)
    url_inputs = html.xpath("//div[@id='url_items']/div/input")
    assert len(url_inputs) == 3
    # remove an entry
    response = await client.get("/add/by_url/1/item/1/0")
    assert response.status_code == 200
    # check there are now two url entries possible
    html = fromstring(response.text)
    url_inputs = html.xpath("//div[@id='url_items']/div/input")
    assert len(url_inputs) == 2

    # post 2 entries
    response = await client.post(
        "/add/by_url/1",
        data={"base_path": "string", "urls": ["http://test/1", "http://test/2"]},
    )
    assert response.status_code == 200
    assert (
        "Failed to add torrent: Could not create download, the input is not a valid torrent."  # noqa: E501
        in response.text
    )


@pytest.mark.anyio
async def test_add_by_file(
    client: AsyncTestClient, rtorrent_port_existing_user: dict[str, str]
) -> None:
    response = await client.get("/add/by_file/1")
    assert response.status_code == 200
    assert response.url == "http://testserver.local/add/by_file/1"
    # check there is a torrent multipart form input
    html = fromstring(response.text)
    files_input = html.xpath("//input[@id='paths']")
    assert len(files_input) == 1
    # post files
    files = []
    for file in (Path(__file__).parent / "torrents").iterdir():
        async with aiofiles.open(file, "rb") as f:
            files.append(("paths", io.BytesIO(await f.read())))
        # f = await aiofiles.open(file, "rb")
        # files.append(("paths", io.BytesIO(await f.read())))
        # await f.close()

    response = await client.post(
        "/add/by_file/1", data={"base_path": "string"}, files=files
    )
    assert response.status_code == 200
