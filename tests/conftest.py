import logging
import os
from pathlib import Path
from typing import AsyncIterator, Generator
from unittest import mock
from urllib.parse import quote, urlparse, urlunparse
from uuid import uuid4

import pytest
from asyncpg import Connection, create_pool
from cappa import Dep
from cappa.testing import CommandRunner
from faker import Faker
from httpx import AsyncClient, MockTransport, Response
from litestar import Litestar
from litestar.testing import AsyncTestClient
from lxml.html import fromstring

from yatg.app import create_app
from yatg.cli import Config, Yatg, get_config
from yatg.settings import AppSettings

logger = logging.getLogger(__name__)

RTORRENT_PORT_DOWNLOAD_BASE_PATH = (
    Path(__file__).parent.parent / "rtorrent/.local/share/rtorrent/download"
)

RTORRENT_SOCKET_DOWNLOAD_BASE_PATH = (
    Path(__file__).parent.parent / "rtorrent_socket/.local/share/rtorrent/download"
)

RTORRENT_SOCKET_SOCKET_PATH = (
    Path(__file__).parent.parent / "rtorrent_socket/.local/share/rtorrent/rtorrent.sock"
)


@pytest.fixture(scope="session")
def anyio_backend() -> str:
    return "asyncio"


@pytest.fixture(scope="session")
def env_test_file() -> Path:
    if os.getenv("CI") == "true":  # pragma: no cover
        env_file = Path(__file__).parent / ".env_ci_gitlab"
        logger.info("CI env file loaded")
    else:
        env_file = Path(__file__).parent / ".env_test"
        logger.info("Local test env file loaded")
    assert env_file.exists()
    return env_file


@pytest.fixture(scope="session")
def app_settings_test(
    env_test_file: Path,
) -> AppSettings:
    return AppSettings.from_env(
        env_test_file,
    )


@pytest.fixture(scope="session")
def runner(app_settings_test: AppSettings) -> CommandRunner:
    _migrations_dir = Path(__file__).parent.parent / "migrations"

    def _get_config() -> Config:
        return Config(
            app_settings=app_settings_test,
            migrations_dir=_migrations_dir,
        )

    return CommandRunner(Yatg, deps={get_config: Dep(_get_config)})


@pytest.fixture(scope="session")
async def admin_connection(
    app_settings_test: AppSettings,
) -> AsyncIterator[Connection]:
    async with create_pool(app_settings_test.postgres_url) as pool:
        async with pool.acquire() as conn:
            yield conn


@pytest.fixture(scope="session")
async def db_connection(
    admin_connection: Connection,
    app_settings_test: AppSettings,
) -> AsyncIterator[Connection]:
    db_name = f'tmp_{str(uuid4()).replace("-", "_")}'
    logger.debug(db_name)
    await admin_connection.execute(f"CREATE DATABASE {db_name}")
    try:
        parsed = urlparse(app_settings_test.postgres_url)
        replaced = parsed._replace(path=db_name)
        dsn_unparse = urlunparse(replaced)
        # important hack
        app_settings_test.postgres_url = dsn_unparse
        async with create_pool(dsn_unparse) as pool:
            async with pool.acquire() as conn:
                yield conn
    except Exception:  # pragma: no cover
        raise
    finally:
        logger.info(f"admin DROP DATABASE {db_name}")
        await admin_connection.execute(f"DROP DATABASE {db_name}")


async def new_client() -> AsyncIterator[AsyncClient]:
    yield AsyncClient(
        transport=MockTransport(lambda _: Response(419, content="FOO NOT FOUND")),
        timeout=5.0,
    )


@pytest.fixture(scope="session")
def mock_tclient() -> Generator:
    with mock.patch("yatg.dependencies.get_tclient", new=new_client):
        yield


@pytest.fixture(scope="session")
async def app_test(
    app_settings_test: AppSettings,
    mock_tclient: AsyncClient,
    db_connection: Connection,
    runner: CommandRunner,
) -> AsyncIterator[Litestar]:
    await runner.invoke_async("migrate")
    app = create_app(app_settings=app_settings_test)
    assert app_settings_test.debug
    yield app


@pytest.fixture(scope="session")
async def client(
    app_test: Litestar, db_connection: Connection
) -> AsyncIterator[AsyncTestClient]:
    async with AsyncTestClient(app_test) as client:
        yield client


@pytest.fixture
async def rtorrent_port_existing_user(
    client: AsyncTestClient,
) -> dict[str, str]:
    fake = Faker()
    email, password = fake.email(), fake.password()
    response = await client.get(
        "/auth/signup",
    )
    assert response.status_code == 200
    # check we have the username, password, and client dropdown
    html = fromstring(response.text)
    assert html.xpath("//input[@name='username']")
    assert html.xpath("//input[@name='password']")
    assert html.xpath("//input[@name='alias']")
    assert html.xpath("//select[@name='client']")
    # check also we can enter a base_path
    assert html.xpath("//input[@name='base_path']")
    # select rtorrent
    response = await client.get(
        "/settings/client?client=rtorrent",
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "Rtorrent connection" in response.text
    # select socket
    response = await client.get(
        "/settings/client/rtorrent?uri_type=tcp",
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "Rtorrent tcp" in response.text
    # enter socket path
    if os.environ.get("CI") == "true":  # pragma: no cover
        uri = "scgi://rtorrent:50005"
    else:
        uri = "scgi://localhost:50005"
    response = await client.get(
        f"/auth/rtorrent_check?uri={uri}",
        follow_redirects=True,
    )
    assert "Save rtorrent settings" in response.text
    assert response.status_code == 200
    # save settings
    response = await client.post(
        "/auth/signup",
        data={
            "base_path": RTORRENT_PORT_DOWNLOAD_BASE_PATH,
            "client": "rtorrent",
            "uri": uri,
            "uri_type": "tcp",
            "checked": "true",
            "username": email,
            "password": password,
            "alias": "my_rtorrent_port",
        },
        follow_redirects=True,
    )
    assert response.status_code == 200
    return {"username": email, "password": password}


@pytest.fixture
async def rtorrent_socket_existing_user(
    client: AsyncTestClient,
) -> dict[str, str]:
    fake = Faker()
    email, password = fake.email(), fake.password()
    response = await client.get(
        "/auth/signup",
    )
    assert response.status_code == 200
    # check we have the username, password, and client dropdown
    html = fromstring(response.text)
    assert html.xpath("//input[@name='username']")
    assert html.xpath("//input[@name='password']")
    assert html.xpath("//input[@name='alias']")
    assert html.xpath("//select[@name='client']")
    # check also we can enter a base_path
    assert html.xpath("//input[@name='base_path']")
    # select rtorrent
    response = await client.get(
        "/settings/client?client=rtorrent",
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "Rtorrent connection" in response.text
    # select socket
    response = await client.get(
        "/settings/client/rtorrent?uri_type=socket",
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "Rtorrent socket" in response.text
    # enter socket path
    if os.environ.get("CI") == "true":  # pragma: no cover
        uri = "scgi://rtorrent:50005"
    else:
        uri = f"scgi+unix://{RTORRENT_SOCKET_SOCKET_PATH}"
    response = await client.get(
        f"/auth/rtorrent_check?uri={quote(uri)}",
        follow_redirects=True,
    )
    assert "Save rtorrent settings" in response.text
    assert response.status_code == 200
    # save settings
    response = await client.post(
        "/auth/signup",
        data={
            "base_path": RTORRENT_SOCKET_DOWNLOAD_BASE_PATH,
            "client": "rtorrent",
            "uri": uri,
            "uri_type": "socket",
            "checked": "true",
            "username": email,
            "password": password,
            "alias": "my_rtorrent_socket",
        },
        follow_redirects=True,
    )
    assert response.status_code == 200
    return {"username": email, "password": password}
