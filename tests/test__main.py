from yatg.settings import AppSettings


def test_app_settings(app_settings_test: AppSettings) -> None:
    assert app_settings_test.debug is True
