import pytest
from litestar.testing import AsyncTestClient
from lxml.html import fromstring


@pytest.mark.anyio
async def test_settings_client_get(client: AsyncTestClient) -> None:
    response = await client.get("/settings")
    assert response.status_code == 200
    # check we have a destination input
    html = fromstring(response.text)
    assert html.xpath("//input[@id='base_path']")
    assert html.xpath("//select[@id='client']")
    # assert select has 3 options: rtorrent, deluge and transmission
    assert html.xpath("//select[@id='client']/*/option/text()") == [
        "rtorrent",
        "deluge",
        "transmission",
    ]
    # select the rtorrent one
    response = await client.get("/settings/client?client=rtorrent")
    assert response.status_code == 200
    html = fromstring(response.text)
    # check we have now tcp and socket choices
    assert html.xpath("//select[@id='uri_type']")
    # assert select has 2 options: tcp and socket
    assert html.xpath("//select[@id='uri_type']/*/option/text()") == [
        "socket",
        "tcp",
    ]
