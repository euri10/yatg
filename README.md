
# Instructions for development

* create a `.env` file with the following content at the top of the hierarchy:
```
YATG_DEBUG=True
YATG_RTORRENT_SOCKET=fake
```
* run dev env with ` docker compose -f docker-compose-dev.yml up -d --build`
* access is on `http://localhost:10000`
* no user/pass is implemented yet
* just fill in the socket path in the form, it should be `scgi+unix:///config/.local/share/rtorrent/rtorrent.sock`

# Demo

![yatg_demo](yatg_demo.gif)
