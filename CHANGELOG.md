# [1.3.0](https://gitlab.com/euri10/yatg/compare/1.2.0...1.3.0) (2024-08-25)


### Features

* **rtorrent:** added a browser directory to select the folder where torrent will be downloaded to ([979ef07](https://gitlab.com/euri10/yatg/commit/979ef077dff3bf03556fff5869d6cbea2ad8ce19))
* **settings:** added the ability to save the download directory base path in settings ([4103627](https://gitlab.com/euri10/yatg/commit/41036279cd3369189c5ee82337d4f98a92947d3b))

# [1.2.0](https://gitlab.com/euri10/yatg/compare/1.1.0...1.2.0) (2024-07-11)


### Bug Fixes

* **deluge:** it's now possible to save th deluge settings ([dfe6255](https://gitlab.com/euri10/yatg/commit/dfe625516ce013615b3900fae6f3414749d56be4))
* **rtorrent:** fix an error when rtorrent data was more than the bytes stream max_bytes ie 65536 ([381b0a2](https://gitlab.com/euri10/yatg/commit/381b0a24bca90a48f2918b05310c91ab512e8a36))
* **rtorrent:** fix an error when rtorrent data was more than the bytes stream max_bytes ie 65536 ([3ad3034](https://gitlab.com/euri10/yatg/commit/3ad3034aaaa4f9970efb249a53bdd17c47c399ba))


### Features

* **deluge:** add a DelugeClient prototype ([5075122](https://gitlab.com/euri10/yatg/commit/5075122845cdedf60c258849345cbcf4f1e55ad0))
* **router:** add possibility to download the .torrent file ([43331da](https://gitlab.com/euri10/yatg/commit/43331da064fac97bb67150dc3d9256cc9ff24a0c))

# [1.1.0](https://gitlab.com/euri10/yatg/compare/1.0.3...1.1.0) (2024-07-08)


### Features

* **router:** add the ability to add more than one url ([e47dcfc](https://gitlab.com/euri10/yatg/commit/e47dcfcf92ac7bcf1b0b36e6d74dc765b49ade45))

## [1.0.3](https://gitlab.com/euri10/yatg/compare/1.0.2...1.0.3) (2024-06-26)


### Bug Fixes

* **settings:** settings is now a modal ([ff9f776](https://gitlab.com/euri10/yatg/commit/ff9f7761d5fa3829d292e93159f30b6d3b5a7133))

## [1.0.2](https://gitlab.com/euri10/yatg/compare/1.0.1...1.0.2) (2024-06-25)


### Bug Fixes

* **router:** fix a bug that let a user save invalid settings ([71405c3](https://gitlab.com/euri10/yatg/commit/71405c3373acf7e7ca57bffec2a00258542b071b))

## [1.0.1](https://gitlab.com/euri10/yatg/compare/1.0.0...1.0.1) (2024-06-24)


### Bug Fixes

* **ci:** use the slug on pytest job ([09e5d1e](https://gitlab.com/euri10/yatg/commit/09e5d1e37b860708e89e0f80950807c4b6a24bab))

# 1.0.0 (2024-06-24)


### Features

* add the 1st version ([3f9710e](https://gitlab.com/euri10/yatg/commit/3f9710e1d34d1dc7088f2825bbf35c9322d0a19a))
