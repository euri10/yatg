FROM registry.hub.docker.com/library/python:3.12-slim-bookworm

RUN apt-get -y update && apt-get -y upgrade \
    && apt-get -y install --no-install-recommends deluged=2.0.3-4 \
    && rm -rf /var/lib/apt/lists/*
COPY ./deluge/auth /root/.config/deluge/auth
COPY ./deluge/core.conf /root/.config/deluge/core.conf
EXPOSE 58846

ENTRYPOINT ["deluged", "-d"]

#CMD ["tail", "-f", "/dev/null"]
