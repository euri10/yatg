# Use Alpine as base image
FROM alpine:3.20.2

# Install bash
RUN apk add --no-cache bash=5.2.26-r0

# Copy all binaries from jesec/rtorrent image
COPY --from=jesec/rtorrent:latest / /

# Set the entrypoint to rtorrent
EXPOSE 50005
ENTRYPOINT ["rtorrent"]
